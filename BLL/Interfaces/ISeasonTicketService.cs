﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ISeasonTicketService
    {
        SeasonTicketModel AddSeasonTicket(SeasonTicketModel seasonTicketModel);
        float GetCost (SeasonTicketModel seasonTicketModel);
        List<SeasonTicketModel> GetSeasonTickets(int clientId);
        List<SeasonTicketModel> GetActiveSeasonTicket(int ServiceID);
        void AddVisiting(int SeasonTicketID);
    }
}
