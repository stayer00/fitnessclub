﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IDbCrud
    {
        List<ClientModel> GetClients();
        ClientModel GetClient(int clientId);
        ClientModel CreateClient(ClientModel client);
        EmployeeModel CreateEmployee(EmployeeModel e, DateTime dateStart, List<ServiceModel> activities, string inputLogin, string inputPassword);
        EmployeeModel UpdateEmployee(EmployeeModel e, List<ServiceModel> activities);
        void DeleteEmployee(int id);
        List<EmployeeModel> GetEmployees();
        EmployeeModel GetEmployee(int employeeId);
        List<TypeEmployeeModel> GetEmployeeTypes();
        List<ServiceModel> GetServices();
        ServiceModel GetService(int idService);
        ServiceModel CreateService(ServiceModel s);
        List<LoungeModel> GetLounges();
        void DeleteService(int id);
        List<CoachModel> GetCoaches();
        CoachModel GetCoach(int idCoach);
        void UpdateClient(ClientModel client);
        void DeleteClient(int id);
        List<SalaryModel> DeliverSalary();
        bool Auth(string login, string password);
    }
}
