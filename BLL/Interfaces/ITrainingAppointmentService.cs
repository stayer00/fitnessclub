﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface ITrainingAppointmentService
    {
        List<TrainingAppointmentModel> GetTrainingAppointments(DateTime date);
        void AddClientToTraining(int SeasonTicketID, int TrainingAppointmentID);
        void DeleteClientTrainingAppointment(int TrainingAppointmentID, int clientId);
        void DeleteTrainingAppointment(int TrainingAppointmentID);
        TrainingAppointmentModel GetLastTrainingAppointment();
        void CreateTrainingAppointment(TrainingAppointmentModel tr);
        bool CheckCanCreateTraininingAppointment(TrainingAppointmentModel tr);
        void ToggleCancelTrA(int id);
    }
}
