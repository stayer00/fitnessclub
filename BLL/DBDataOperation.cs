﻿using BLL.Interfaces;
using BLL.Models;
using DAL;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class DbDataOperation : IDbCrud
    {
        IDbRepos dbr;
        //private FitnessClubModel db;
        public DbDataOperation(IDbRepos repos)
        {
            dbr = repos;
            //db = new FitnessClubModel();
            //db.SeasonTickets.Load();
        }

        //public DbDataOperation()
        //{
        //    db = new FitnessClubModel();
        //    db.SeasonTickets.Load();
        //}

        public List<ClientModel> GetClients()
        {
            return dbr.Clients.GetList().Select(i=>new ClientModel(i)).ToList();
        }

        public ClientModel GetClient(int id)
        {
            return new ClientModel(dbr.Clients.GetItem(id));
        }

        public ClientModel CreateClient(ClientModel client)
        {
            var lastHuman = dbr.Human.GetLast();
            int maxId = lastHuman == null ? 1 : lastHuman.ID + 1;

            dbr.Human.Create(new Human() { 
                ID = maxId,
                name = client.Name,
                surname = client.Surname,
                gender = client.Gender,
                birthday = client.Birthday,
                numberPhone = client.NumberPhone 
            });

            dbr.Clients.Create(new Client() { ID = maxId, dateRegistry = DateTime.Now });
            dbr.Save();
            return GetClient(maxId);
        }

        public List<EmployeeModel> GetEmployees()
        {
            return dbr.Employees.GetList().Select(i => new EmployeeModel(i)).ToList();
        }

        public List<TypeEmployeeModel> GetEmployeeTypes()
        {
            return dbr.Employees.GetEmployeeTypes().Select(i => new TypeEmployeeModel(i)).ToList();
        }

        public EmployeeModel GetEmployee(int employeeId)
        {
            return new EmployeeModel(dbr.Employees.GetItem(employeeId));
        }

        //public List<string> GetTrainingAppointmentClients(int TrainingAppointmentID)
        //{
        //    return db.AppointmentClientTrainings.Where(i => i.TrainingAppointmentID == TrainingAppointmentID).Select(i => i.SeasonTicket.Client.Human.surname).ToList();
        //}

        public Employee GetEmployeeItem(int id)
        {
            return dbr.Employees.GetItem(id);
        }

        public ServiceModel CreateService(ServiceModel s)
        {
            var last = dbr.Services.GetList().OrderByDescending(u => u.ID).FirstOrDefault();
            int maxId = last == null ? 1 : last.ID + 1;
            var service = new Service() { ID = maxId, name = s.Name, cost = s.Cost};
            dbr.Services.Create(service);
            dbr.Save();
            return new ServiceModel(dbr.Services.GetLast());
        }

        public EmployeeModel CreateEmployee(EmployeeModel e, DateTime dateStart, List<ServiceModel> activities, string inputLogin, string inputPassword)
        {
            var lastHuman = dbr.Human.GetLast();
            int maxId = lastHuman == null ? 1 : lastHuman.ID + 1;
            var humen = new Human() { ID = maxId, name = e.Name, surname = e.Surname, gender = e.Gender, birthday = e.Birthday, numberPhone = e.NumberPhone };
            dbr.Human.Create(humen);
            //var humen = db.Humen.OrderByDescending(c => c.ID).First();
            dbr.Employees.Create(new Employee() {
                ID = maxId,
                getStarted = e.GetStarted,
                address = e.Address,
                baseSalary = e.BaseSalary,
                employmentRecordNumber = e.EmploymentRecordNumber,
                ITN = e.ITN,
                passport = e.Passport,
                snils = e.Snils,
                TypeEmployeeID = e.TypeEmployeeID,
            });

            if (inputPassword != "" && inputLogin != "")
            {
                dbr.Employees.CreateSystemUser(new UserSystem()
                {
                    ID = maxId,
                    email = inputLogin,
                    password = inputPassword
                });
            }

            if (e.TypeEmployeeID == 1)
            {
                dbr.Coaches.Create(new Coach()
                {
                    ID = maxId,
                    dateStartWorkExpirience = dateStart
                });
                foreach (var activity in activities)
                {
                    dbr.Coaches.AddCoachActivity(activity.Id, maxId);
                }
            }

            dbr.Save();
            return new EmployeeModel(GetEmployeeItem(dbr.Human.GetLast().ID));
        }

        public EmployeeModel UpdateEmployee(EmployeeModel e, List<ServiceModel> activities)
        {
            var employee = dbr.Employees.GetItem(e.ID);
            employee.ITN = e.ITN;
            employee.passport = e.Passport;
            employee.snils = e.Snils;
            employee.employmentRecordNumber = e.EmploymentRecordNumber;
            employee.address = e.Address;
            employee.baseSalary = e.BaseSalary;
            //var humen = db.Humen.OrderByDescending(c => c.ID).First();
            employee.Human.name = e.Name;
            employee.Human.surname = e.Surname;
            employee.Human.birthday = e.Birthday;
            employee.Human.gender = e.Gender;
            employee.Human.numberPhone = e.NumberPhone;

            dbr.Employees.Update(employee);

            if (e.TypeEmployeeID == 1)
            {
                dbr.Coaches.DeleteCoachActivities(e.ID);
                foreach (var activity in activities)
                {
                    dbr.Coaches.AddCoachActivity(activity.Id, e.ID);
                }
            }

            dbr.Save();
            return new EmployeeModel(GetEmployeeItem(e.ID));
        }

        public void UpdateClient(ClientModel cl)
        {
            Client client = dbr.Clients.GetItem(cl.ID);
            client.Human.name = cl.Name;
            client.Human.surname = cl.Surname;
            client.Human.birthday = cl.Birthday;
            client.Human.gender = cl.Gender;
            client.Human.numberPhone = cl.NumberPhone;
            dbr.Clients.Update(client);
            dbr.Save();
        }

        public void DeleteClient(int id)
        {
            dbr.Clients.Delete(id);
            dbr.Save();
        }

        public void DeleteService(int id)
        {
            dbr.Services.Delete(id);
            dbr.Save();
        }

        public void DeleteEmployee(int id)
        {
            dbr.Employees.Delete(id);
            dbr.Save();
        }


        public List<ServiceModel> GetServices()
        {
            return dbr.Services.GetList().Select(i=>new ServiceModel(i)).ToList();
        }

        public List<CoachModel> GetCoaches()
        {
            return dbr.Coaches.GetList().Select(i => new CoachModel(i)).ToList();
        }

        public List<LoungeModel> GetLounges()
        {
            return dbr.Lounges.GetList().Select(i => new LoungeModel(i)).ToList();
        }

        public ServiceModel GetService(int idService)
        {
            return new ServiceModel(dbr.Services.GetItem(idService));
        }

        public CoachModel GetCoach(int idCoach)
        {
            return new CoachModel(dbr.Coaches.GetItem(idCoach));
        }

        public List<SalaryModel> DeliverSalary()
        {
            var salaries = dbr.Employees.DeliverSalary().Select(i => new SalaryModel(i)).ToList();
            dbr.Save();
            return salaries;
        }

        public bool Auth(string login, string password)
        {
            if (dbr.Employees.GetSystemUser(login, password) == null)
            {
                return false;
            }
            return true;
        }
    }
}
