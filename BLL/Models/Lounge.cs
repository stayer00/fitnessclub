﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class LoungeModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int MaxCount { get; set; }
        public LoungeModel() { }
        public LoungeModel(Lounge s)
        {
            Id = s.ID;
            Name = s.name;
            MaxCount = s.maxCount;
        }
    }
}
