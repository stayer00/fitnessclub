﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ServiceModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }

        public ServiceModel() { }
        public ServiceModel(Service s)
        {
            Id = s.ID;
            Name = s.name;
            Cost = s.cost;
        }
    }
}
