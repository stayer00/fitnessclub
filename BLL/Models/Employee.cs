﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class EmployeeModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime Birthday { get; set; }
        public string NumberPhone { get; set; }
        public DateTime GetStarted { get; set; }
        public string Address { get; set; }
        public float BaseSalary { get; set; }
        public string ITN { get; set; }
        public string Passport { get; set; }
        public string Snils { get; set; }
        public string EmploymentRecordNumber { get; set; }
        public int TypeEmployeeID { get; set; }

        public EmployeeModel() { }
        public EmployeeModel(Employee e) {
            ID = e.ID;
            Name = e.Human.name;
            Surname = e.Human.surname;
            Gender = e.Human.gender;
            Birthday = e.Human.birthday;
            NumberPhone = e.Human.numberPhone;
            GetStarted = e.getStarted;
            Address = e.address;
            BaseSalary = e.baseSalary;
            ITN = e.ITN;
            Passport = e.passport;
            Snils = e.snils;
            TypeEmployeeID = e.TypeEmployeeID;
            EmploymentRecordNumber = e.employmentRecordNumber;
        }
    }
}
