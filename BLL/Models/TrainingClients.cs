﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TrainingClientsModel
    {
        public int Id { get; set; }
        public int ClientID { get; set; }
        public ClientModel Client { get; set; }
        public TrainingClientsModel() { }
        public TrainingClientsModel(AppointmentClientTraining clT)
        {
            Id = clT.ID;
            ClientID = clT.SeasonTicket.ClientID;
            Client = new ClientModel(clT.SeasonTicket.Client);
        }
    }
}
