﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TypeEmployeeModel
    {
        public int ID { get; set; }

        public string Name { get; set; }
        public TypeEmployeeModel(TypeEmployee t) {
            ID = t.ID;
            Name = t.Name;
        }
    }
}
