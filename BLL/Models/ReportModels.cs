﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class LineServiceReport
    {
        public string Name { get; set; }
        public float? Cost { get; set; }
        public int Month { get; set; }
    }

    public class ServiceReport
    {
        public List<Dictionary<int,float>> Report { get; set; }
    }
}
