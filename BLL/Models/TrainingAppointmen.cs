﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class TrainingAppointmentModel
    {
        public int Id { get; set; }
        public int LoungeID { get; set; }
        public int ServiceID { get; set; }
        public int CoachID { get; set; }
        public DateTime Date { get; set; }
        public int MaxCount { get; set; }
        public int Count { get; set; }
        public Service Service { get; set; }
        public Coach Coach { get; set; }
        public Lounge Lounge { get; set; }
        public bool IsClosed { get; set; }
        public bool IsCanceled { get; set; }
        public List<AppointmentClientTraining> AppointmentClientTraining { get; set; }
        public List<SeasonTicketModel> TrainingClients { get; set; }
        public TrainingAppointmentModel() { }
        public TrainingAppointmentModel(TrainingAppointment tr)
        {
            Id = tr.ID;
            LoungeID = tr.LoungeID;
            ServiceID = tr.ServiceID;
            CoachID = tr.CoachID;
            Date = tr.date;
            MaxCount = tr.maxCount;
            Service = tr.Service;
            Coach =  tr.Coach;
            Lounge = tr.Lounge;
            Count = tr.AppointmentClientTrainings.Count();
            IsCanceled = tr.IsCanceled;
            IsClosed = tr.AppointmentClientTrainings.Count() == tr.maxCount;
            //ICollection<ClientModel> trC = new List<ClientModel>();
            //if (tr.AppointmentClientTrainings.Select(i => new ClientModel(i.SeasonTicket.Client)).Count() != 0)
            //{
            //    trC = tr.AppointmentClientTrainings.Select(i => new ClientModel(i.SeasonTicket.Client));
            //}
            //TrainingClients = trC;
        }
    }
}
