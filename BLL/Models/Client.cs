﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class ClientModel
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public string Surname { get; set; }

        public string Gender { get; set; }

        public DateTime Birthday { get; set; }
        public string NumberPhone { get; set; }
        public DateTime DateRegistry { get; set; }

        public ClientModel() { }
        public ClientModel(Client cl) {
            ID = cl.ID;
            Name = cl.Human.name;
            Surname = cl.Human.surname;
            Gender = cl.Human.gender;
            Birthday = cl.Human.birthday;
            NumberPhone = cl.Human.numberPhone;
            DateRegistry = cl.dateRegistry;
        }
    }
}
