﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Models
{
    public class ActivityModel
    {
        public int idService { get; set; }
        public string nameServise { get; set; }
        public int idCoach { get; set; }
        public ActivityModel(DAL.Activity activity)
        {
            idCoach = activity.CoachID;
            nameServise = activity.Service.name;
            idService = activity.ServiceID;
        }
    }
}
