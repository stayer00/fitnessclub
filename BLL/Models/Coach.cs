﻿using BLL.Models;
using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class CoachModel
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string Name { get; set; }
        public List<ActivityModel> Activities { get; set; }

        public CoachModel() { }
        public CoachModel(Coach c)
        {
            if (c != null)
            {
                Id = c.ID;
                Surname = c.Employee.Human.surname;
                Name = c.Employee.Human.name;
                Activities = c.Activities.Select(i => new ActivityModel(i)).ToList();
            }
            else
            {
                Id = -1;
                Surname = "";
                Name = "";
                Activities = new List<ActivityModel>();
            }
        }
    }
}
