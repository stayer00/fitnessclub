﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class SalaryModel
    {
        public string Surname { get; set; }
        public float Salary { get; set; }

        public SalaryModel() { }
        public SalaryModel(Salary s)
        {
            Surname = s.Employee.Human.surname;
            Salary = s.salary1;
        }
    }
}
