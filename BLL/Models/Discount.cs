﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class Discount
    {
        public Discount(DateTime birthday, int totalClasses, int totalVisiting)
        {
            int saleAge = getSaleAge(birthday);
            int saleClasses = getSaleClasses(totalClasses);
            int saleConstantClient = getSaleConstatntClient(totalVisiting);

            value = (saleAge + saleClasses + saleConstantClient) >= 50 ? 50 : (saleAge + saleClasses + saleConstantClient);
        }

        private int value = 0;
        public int Value { get { return value; } }

        public float GetDiscountedPrice(double totalCost)
        {
            return (float)(Math.Round((totalCost - totalCost * ((Convert.ToDouble(value) / 100.0)))));
        }

        private static int FullYears(DateTime birthday)
        {
            int nowYear = DateTime.Now.Year;
            int birthdayYear = birthday.Year;
            if (nowYear <= birthdayYear)
                return 0;
            int n = nowYear - birthdayYear;
            if (birthday.DayOfYear > DateTime.Now.DayOfYear)
                --n;
            return n;
        }

        public static int getSaleAge(DateTime birthday)
        {
            return FullYears(Convert.ToDateTime(birthday)) >= 60 ? 10 : 0;
        }

        public static int getSaleClasses(int totalClasses)
        {
            return totalClasses >= 50 ? 50 : (totalClasses >= 25 ? 25 : (totalClasses >= 10 ? 10 : 0));
        }

        public static int getSaleConstatntClient(int totalVisiting)
        {
            return totalVisiting >= 75 ? 20 : 0;
        }
    }
}
