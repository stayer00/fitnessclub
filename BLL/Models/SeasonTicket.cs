﻿using DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public class SeasonTicketModel
    {
        public int ID { get; set; }

        public int ServiceID { get; set; }

        public int ClientID { get; set; }

        public int? CoachID { get; set; }

        public int classesTotal { get; set; }

        public int classesLeft { get; set; }
        public double sale { get; set; }

        public DateTime dateStart { get; set; }

        public DateTime dateEnd { get; set; }

        public float totalCost { get; set; }

        public int Count { get; set; }
        public ClientModel Client{ get; set; }
        public ServiceModel Service { get; set; }
        public CoachModel Coach { get; set; }
        public SeasonTicketModel() { }
        public SeasonTicketModel(SeasonTicket st)
        {
            ID = st.ID;
            ServiceID = st.ServiceID;
            ClientID = st.ClientID;
            CoachID = st.CoachID;
            classesTotal = st.classesTotal;
            classesLeft = st.classesLeft;
            sale = Math.Round((1 - Convert.ToDouble(st.totalCost) / (float)(st.Service.cost * st.classesTotal)) * 100);
            dateStart = st.dateStart;
            dateEnd = st.dateEnd;
            totalCost = st.totalCost;
            Client = new ClientModel(st.Client);
            Coach = new CoachModel(st.Coach);
            Service = new ServiceModel(st.Service);
        }
    }
}
