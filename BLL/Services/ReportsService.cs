﻿using BLL.Interfaces;
using BLL.Models;
using DAL;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReportService : IReportService
    {
        private IDbRepos db;
        public ReportService(IDbRepos repos)
        {
            db = repos;
        }
        public Dictionary<string, List<float>> ReportByServices(int year)
        {
            var query = db.Report.GetSTwithServices(year)
                .Select(i => new LineServiceReport() { Name = i.Service.name, Month = i.dateStart.Month, Cost = i.totalCost, })
                .GroupBy(i => new { i.Name, i.Month })
                .Select((i) => new LineServiceReport() { Name = i.Key.Name, Month = i.Key.Month, Cost = i.Sum(j => j.Cost) })
                .ToList();

            var services = db.Services.GetList().Select(i => new ServiceModel(i));
            var report = new Dictionary<string, List<float>>();

            foreach (var service in services)
            {
                List<float> rev = new List<float>();
                for (int i = 1; i <= 12; i++)
                {
                    var findService = query.Where(s => s.Name == service.Name && s.Month == i).FirstOrDefault();
                    if (findService != null)
                    {
                        rev.Add(findService.Cost != null ? (float)findService.Cost : 0);
                    }
                    else
                    {
                        rev.Add(0);
                    }
                }

                report.Add(service.Name, rev);
            }

            return report;
        }
    }
}
