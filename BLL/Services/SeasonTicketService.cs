﻿using BLL.Interfaces;
using DAL;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class SeasonTicketService : ISeasonTicketService
    {
        private IDbRepos db;
        public SeasonTicketService(IDbRepos repos)
        {
            db = repos;
        }

        public float GetCost(SeasonTicketModel st)
        {
            var client = db.Clients.GetItem(st.ClientID);
            var service = db.Services.GetItem(st.ServiceID);
            var totalVisiting = db.Clients.GetClientVisiting(client.ID);

            return new Discount(client.Human.birthday, st.classesTotal, totalVisiting).GetDiscountedPrice(service.cost * st.classesTotal);
        }

        public SeasonTicketModel AddSeasonTicket(SeasonTicketModel st)
        {
            var client = db.Clients.GetItem(st.ClientID);
            var service = db.Services.GetItem(st.ServiceID);
            var totalVisiting = db.Clients.GetClientVisiting(client.ID);

            st.totalCost = new Discount(client.Human.birthday, st.classesTotal, totalVisiting).GetDiscountedPrice(service.cost * st.classesTotal);

            SeasonTicket seasonTicket = new SeasonTicket
            {
                ServiceID = st.ServiceID,
                ClientID = st.ClientID,
                CoachID = st.CoachID == null ? null : st.CoachID,
                classesTotal = st.classesTotal,
                classesLeft = st.classesTotal,
                dateStart = st.dateStart,
                dateEnd = st.dateEnd,
                saleClasses = Discount.getSaleClasses(st.classesTotal),
                saleConstantClient = Discount.getSaleConstatntClient(totalVisiting),
                saleAge = Discount.getSaleAge(client.Human.birthday),
                totalCost = st.totalCost
            };

            db.SeasonTickets.CreateSeasonTicket(seasonTicket);
            if (db.Save() > 0)
                return new SeasonTicketModel(seasonTicket);
            return null;
        }

        public void AddVisiting(int SeasonTicketID)
        {
            db.SeasonTickets.AddVisiting(SeasonTicketID);
            db.Save();
        }

        public List<SeasonTicketModel> GetActiveSeasonTicket(int ServiceID)
        {
           return db.SeasonTickets.GetActiveSeasonTicketByService(ServiceID).Select(i => new SeasonTicketModel(i)).ToList();
        }

        public List<SeasonTicketModel> GetSeasonTickets(int clientId)
        {
            var query = db.SeasonTickets.GetClientSeasonTicket(clientId);
            var result = query.Select((g) => new SeasonTicketModel() { 
                ID = g.Select(i => i.ID).FirstOrDefault(),
                classesTotal = g.Select(i => i.classesTotal).FirstOrDefault(),
                classesLeft = g.Select(i => i.classesLeft).FirstOrDefault(),
                dateStart = g.Select(i => i.dateStart).FirstOrDefault(),
                dateEnd = g.Select(i => i.dateEnd).FirstOrDefault(),
                totalCost = g.Select(i => i.totalCost).FirstOrDefault(),
                ServiceID = g.Select(i => i.ServiceID).FirstOrDefault(),
                CoachID = g.Select(i => i.CoachID).FirstOrDefault(),
                Count = g.Select(i => i.Visitings.Count).FirstOrDefault(),
                ClientID = g.Select(i => i.ClientID).FirstOrDefault(),
            });

            return result.ToList().Select(i => new SeasonTicketModel()
            {
                ID = i.ID,
                classesTotal = i.classesTotal,
                classesLeft = i.classesLeft,
                dateEnd = i.dateEnd,
                dateStart = i.dateStart,
                ServiceID = i.ServiceID,
                CoachID = i.CoachID,
                ClientID = i.ClientID,
                totalCost = i.totalCost,
                Service = new ServiceModel(db.Services.GetItem(i.ServiceID)),
                Coach = new CoachModel(db.Coaches.GetItem(i.CoachID)),
                Client = new ClientModel(db.Clients.GetItem(i.ClientID)),
                Count = i.Count
            }).ToList();
        }
    }
}
