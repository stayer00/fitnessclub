﻿using BLL.Interfaces;
using DAL;
using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class TrainingAppointmentService : ITrainingAppointmentService
    {
        private IDbRepos db;
        public TrainingAppointmentService(IDbRepos repos)
        {
            db = repos;
        }

        public void AddClientToTraining(int SeasonTicketID, int TrainingAppointmentID)
        {
            db.TrainingAppointments.AddClientToTraining(SeasonTicketID, TrainingAppointmentID);
            db.Save();
        }

        public void DeleteTrainingAppointment(int TrainingAppointmentID)
        {
            db.TrainingAppointments.Delete(TrainingAppointmentID);
            db.Save();
        }
        public void DeleteClientTrainingAppointment(int TrainingAppointmentID, int clientId)
        {
            db.TrainingAppointments.DeleteClientTrainingAppointment(TrainingAppointmentID, clientId);
            db.Save();
        }

        public List<TrainingAppointmentModel> GetTrainingAppointments(DateTime date)
        {
            var query = db.TrainingAppointments.GetTrainingAppointments(date)
                .Select(g => new TrainingAppointmentModel()
                {
                    Id = g.Select(i => i.ID).FirstOrDefault(),
                    MaxCount = g.Select(i => i.maxCount).FirstOrDefault(),
                    Date = g.Select(i => i.date).FirstOrDefault(),
                    Service = g.Select(i => i.Service).FirstOrDefault(),
                    ServiceID = g.Select(i => i.ServiceID).FirstOrDefault(),
                    Lounge = g.Select(i => i.Lounge).FirstOrDefault(),
                    Coach = g.Select(i => i.Coach).FirstOrDefault(),
                    Count = g.Select(i => i.AppointmentClientTrainings.Count).FirstOrDefault(),
                    IsCanceled = g.Select(i => i.IsCanceled).FirstOrDefault(),
                    IsClosed = g.Select(i => i.maxCount).FirstOrDefault() == g.Select(i => i.AppointmentClientTrainings).FirstOrDefault().Count(),
                    AppointmentClientTraining = g.Select(i => i.AppointmentClientTrainings).FirstOrDefault().ToList()//.Select(j => new ClientModel(j.SeasonTicket.Client))//.Select(j => new ClientModel(j.SeasonTicket.Client))).FirstOrDefault()
                });

            var result = new List<TrainingAppointmentModel>();
            foreach (var item in query.ToList())
            {
                item.TrainingClients = item.AppointmentClientTraining.Select(i => new SeasonTicketModel(i.SeasonTicket)).ToList();
                result.Add(item);
            }
            return result;
        }

        public TrainingAppointmentModel GetLastTrainingAppointment()
        {
            return new TrainingAppointmentModel(db.TrainingAppointments.GetLast());
        }

        public void CreateTrainingAppointment(TrainingAppointmentModel tr)
        {
            db.TrainingAppointments.Create(new TrainingAppointment()
            {
                CoachID = tr.CoachID,
                maxCount = tr.MaxCount,
                LoungeID = tr.LoungeID,
                ServiceID = tr.ServiceID,
                date = tr.Date,
            });

            db.Save();
        }

        public bool CheckCanCreateTraininingAppointment(TrainingAppointmentModel tr)
        {
            var trainingsByDay = db.TrainingAppointments.GetTrainingsDay(tr.Date.Date).Select(i => new TrainingAppointmentModel(i)).ToList();
            var a = tr.Date.AddHours(-2);
            var b = tr.Date.AddHours(2);
            var trainingInLounge = trainingsByDay.Where(i => i.LoungeID == tr.LoungeID && tr.LoungeID != 2).ToList().Where(i => tr.Date.AddHours(-2) <= i.Date && i.Date <= tr.Date.AddHours(2)).Count();
            var trainingCoach = trainingsByDay.Where(i => i.CoachID == tr.CoachID).ToList().Where(i => tr.Date.AddHours(-2) <= i.Date && i.Date <= tr.Date.AddHours(2)).Count();

            return ((trainingInLounge == 0) && (trainingCoach == 0)) || ((tr.LoungeID == 2) && (trainingCoach == 0));

        }

        public void ToggleCancelTrA(int id)
        {
            db.TrainingAppointments.ToggleCancelTrA(id);
            db.Save();
        }
    }
}
