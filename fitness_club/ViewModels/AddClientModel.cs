﻿using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static fitness_club.AddClient;

namespace fitness_club.ViewModels
{
    class AddClientModel: INotifyPropertyChanged, IDataErrorInfo
    {
        private List<string> genders;
        private string selectedGender;
        private string inputName;
        private string inputSurname;
        private string inputNumberPhone;
        private DateTime inputBirthday;

        public event CreateHandler AddClient;

        IDbCrud crudServ;
        public bool isOpen;
        public AddClientModel()
        {
            crudServ = WindowStartAdmin.crudServ;

            genders = new List<string>()
            {
                "Man", "Women"
            };
            inputName = inputSurname = inputNumberPhone = "";
            inputBirthday = DateTime.Today;
            IsOpen = false;
            selectedGender = genders[0];
        }

        public List<string> Genders
        {
            get
            {
                return this.genders;
            }
        }

        public string SelectedGender
        {
            get
            {
                return this.selectedGender;
            }
            set
            {
                this.selectedGender = value;
                OnPropertyChanged("SelectedGender");
            }
        }

        public string InputName
        {
            get
            {
                return this.inputName;
            }
            set
            {
                this.inputName = value;
                OnPropertyChanged("InputName");
            }
        }

        public string InputSurname
        {
            get
            {
                return this.inputSurname;
            }
            set
            {
                this.inputSurname = value;
                OnPropertyChanged("InputSurname");
            }
        }

        public string InputNumberPhone
        {
            get
            {
                return this.inputNumberPhone;
            }
            set
            {
                this.inputNumberPhone = value;
                OnPropertyChanged("InputNumberPhone");
            }
        }

        public DateTime InputBirthday
        {
            get
            {
                return this.inputBirthday;
            }
            set
            {
                this.inputBirthday = value;
                OnPropertyChanged("InputBirthday");
            }
        }

        public bool IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        private RelayCommand createClient;
        public RelayCommand CreateClient
        {
            get
            {
                return createClient ??
                  (createClient = new RelayCommand(obj =>
                  {
                      var client = new BLL.ClientModel()
                      {
                          Name = inputName,
                          Surname = inputSurname,
                          Birthday = inputBirthday,
                          Gender = selectedGender,
                          NumberPhone = inputNumberPhone
                      };
                      this.IsOpen = false;
                      AddClient(crudServ.CreateClient(client));
                  },
                  (obj) => (InputName.Length > 1 && InputSurname.Length > 1 && InputBirthday <= DateTime.Now.AddYears(-5) && InputNumberPhone.Length == 11)));
            }
        }

        private RelayCommand toogleDialog;
        public RelayCommand ToggleDialog
        {
            get
            {
                return toogleDialog ??
                  (toogleDialog = new RelayCommand(obj =>
                  {
                      this.IsOpen = !this.IsOpen;
                  }));
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "InputName":
                        if (InputName.Length < 2)
                        {
                            error = "Length of name must be more than 1";
                        }
                        break;
                    case "InputSurname":
                        if (InputSurname.Length < 2)
                        {
                            error = "Length of surname must be more than 1";
                        }
                        break;
                    case "InputNumberPhone":
                        if (InputNumberPhone.Length != 11)
                        {
                            error = "Length of number phone must be equals 11";
                        }
                        break;
                    case "InputBirthday":
                        if (InputBirthday > DateTime.Now.AddYears(-5))
                        {
                            error = "Invalid date";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
