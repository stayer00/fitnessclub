﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace fitness_club.ViewModels
{
    class AddingRecordTrainingModel : INotifyPropertyChanged, IDataErrorInfo
    {
        public ObservableCollection<LoungeModel> lounges { get; private set; }
        private ObservableCollection<CoachModel> coaches;
        public ObservableCollection<ServiceModel> services { get; private set; }

        private LoungeModel selectedLounge;
        private CoachModel selectedCoach;
        private ServiceModel selectedService;
        private DateTime selectedDate;
        private DateTime selectedTime;
        private int maxCount;
        public bool isOpen;

        ITrainingAppointmentService trServ;
        IDbCrud crudServ;

        Timer timer = new Timer(3000);
        public AddingRecordTrainingModel()
        {
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;

            IsOpen = false;
            trServ = WindowStartAdmin.trServ;
            crudServ = WindowStartAdmin.crudServ;
            selectedDate = DateTime.Now;
            selectedTime = new DateTime();

            lounges = new ObservableCollection<LoungeModel>();
            coaches = new ObservableCollection<CoachModel>();
            services = new ObservableCollection<ServiceModel>();
            foreach (var i in crudServ.GetLounges())
            {
                lounges.Add(i);
            }

            foreach (var i in crudServ.GetCoaches())
            {
                coaches.Add(i);
            }

            foreach (var i in crudServ.GetServices())
            {
                services.Add(i);
            }

            selectedLounge = lounges[0] == null ? null : lounges[0];
            selectedService = services[0] == null ? null : services[0];
            selectedCoach = coaches[0] == null ? null : coaches[0];

            isEnabledCreate = true;

            maxCount = 0;
        }

        public ObservableCollection<CoachModel> Coaches
        {
            get
            {
                var newCoaches = new ObservableCollection<CoachModel>();
                foreach (var coach in coaches.Where(i => i.Activities.Where(j => j.idService == selectedService.Id).FirstOrDefault() != null).ToList())
                {
                    newCoaches.Add(coach);
                }
                SelectedCoach = newCoaches[0];
                return newCoaches;
            }
            set
            {
                coaches = value;
            }
        }

        public LoungeModel SelectedLounge
        {
            get
            {
                return this.selectedLounge;
            }
            set
            {
                this.selectedLounge = value;
                OnPropertyChanged("SelectedLounge");
            }
        }


        public CoachModel SelectedCoach
        {
            get
            {
                return this.selectedCoach;
            }
            set
            {
                this.selectedCoach = value;
                OnPropertyChanged("SelectedCoach");
            }
        }

        public ServiceModel SelectedService
        {
            get
            {
                return this.selectedService;
            }
            set
            {
                this.selectedService = value;
                OnPropertyChanged("SelectedService");
                OnPropertyChanged("Coaches");
            }
        }

        public DateTime SelectedDate
        {
            get
            {
                return this.selectedDate;
            }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"DatePicker {DateTime.Now} set date");
                }
                this.selectedDate = value;
                OnPropertyChanged("SelectedDate");
            }
        }

        public DateTime SelectedTime
        {
            get
            {
                return this.selectedTime;
            }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"TimePicker {DateTime.Now} set time");
                }
                this.selectedTime = value;
                OnPropertyChanged("SelectedTime");
            }
        }

        public int MaxCount
        {
            get
            {
                return this.maxCount;
            }
            set
            {
                maxCount = value;
                OnPropertyChanged("MaxCount");
            }

        }

        public bool IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        private bool isEnabledCreate;
        public bool IsEnabledCreate
        {
            get
            {
                return isEnabledCreate;
            }
            set
            {
                isEnabledCreate = value;
                OnPropertyChanged("IsEnabledCreate");
            }
        }

        private int countLounge {
            get
            {
                return lounges.Where(i => i.Id == selectedLounge.Id).FirstOrDefault().MaxCount;
            }
        }

        private RelayCommand createTrainingAppointment;
        public RelayCommand CreateTrainingAppointment
        {
            get
            {
                return createTrainingAppointment ??
                  (createTrainingAppointment = new RelayCommand(obj =>
                  {
                      var training = new BLL.TrainingAppointmentModel()
                      {
                          CoachID = selectedCoach.Id,
                          ServiceID = selectedService.Id,
                          LoungeID = selectedLounge.Id,
                          MaxCount = maxCount,
                          Date = new DateTime(selectedDate.Year, selectedDate.Month, selectedDate.Day, selectedTime.Hour, selectedTime.Minute, selectedTime.Second)
                      };
                      if (trServ.CheckCanCreateTraininingAppointment(training))
                      {
                          trServ.CreateTrainingAppointment(training);
                          IsOpen = false;
                      }
                      else
                      {
                          IsEnabledCreate = false;

                          timer.Start();
                      }
                  },
                  (obj) => SelectedDate.Date >= DateTime.Today && (SelectedCoach.Surname != "-") && MaxCount > 0 && countLounge >= maxCount &&
                  ((SelectedDate.Date != DateTime.Today && SelectedTime.Hour >= 9 && SelectedTime.Hour <= 22) || (SelectedDate.Date == DateTime.Today && SelectedTime.Hour >= DateTime.Now.Hour && SelectedTime.Hour <= 22))));
            }
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            IsEnabledCreate = true;
            timer.Stop();
        }
        private RelayCommand toogleDialog;
        public RelayCommand ToggleDialog
        {
            get
            {
                return toogleDialog ??
                  (toogleDialog = new RelayCommand(obj =>
                  {
                      this.IsOpen = !this.IsOpen;
                  }));
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "SelectedDate":
                        if (SelectedDate.Date < DateTime.Today)
                        {
                            error = "Selected date must be in future";
                        }
                        break;
                    case "SelectedCoach":
                        if (SelectedCoach.Surname == "-")
                        {
                            error = "You should appoint a coach";
                        }
                        break;
                    case "MaxCount":
                        if (MaxCount <= 0)
                        {
                            error = "Max count of clients must be more 0";
                        }
                        if (countLounge < MaxCount)
                        {
                            error = "Max count of clients must be less than max count lounge";
                        }
                        break;
                    case "SelectedTime":
                        if (SelectedTime.Hour < 9 || SelectedTime.Hour > 22)
                        {
                            error = "Time must be in range 9 - 22";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
