﻿using BLL;
using BLL.Interfaces;
using fitness_club.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace fitness_club.ViewModels
{
    class ServicesModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private ObservableCollection<SettingBtn> allServices;
        bool isOpen;
        string inputName;
        int inputCost;
        IDbCrud crudServ;   
        public ServicesModel()
        {
            crudServ = WindowStartAdmin.crudServ;
            _viewId = Guid.NewGuid();
            allServices = new ObservableCollection<SettingBtn>();

            inputName = "";
            inputCost = 100;

            foreach (var item in crudServ.GetServices())
                allServices.Add(new SettingBtn(item));
            SelectedService = allServices[0];
            
        }

        private Guid _viewId;
        public Guid ViewID
        {
            get { return _viewId; }
        }

        private RelayCommand removeService;
        public RelayCommand RemoveService
        {
            get
            {
                return removeService ??
                    (removeService = new RelayCommand(obj =>
                    {
                        SettingBtn service = (SettingBtn)obj;
                        crudServ.DeleteService(service.Id);
                        this.AllServices.Remove(service);
                    }));
            }
        }

        private RelayCommand createService;
        public RelayCommand CreateService
        {
            get
            {
                return createService ??
                  (createService = new RelayCommand(obj =>
                  {
                      var service = new BLL.ServiceModel()
                      {
                          Name = inputName,
                          Cost = inputCost,
                      };
                      this.IsOpen = false;
                      service = crudServ.CreateService(service);
                      allServices.Add(new SettingBtn(service));
                  },
                  (obj) => (inputName.Length > 1 && InputCost > 0)));
            }
        }

        public ObservableCollection<SettingBtn> AllServices
        {
            get
            {
                return allServices;
            }
        }
        
        public string InputName
        {
            get
            {
                return this.inputName;
            }
            set
            {
                this.inputName = value;
                OnPropertyChanged("InputName");
            }
        }

        public int InputCost
        {
            get
            {
                return this.inputCost;
            }
            set
            {
                this.inputCost = value;
                OnPropertyChanged("InputCost");
            }
        }

        public bool IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        private RelayCommand toogleDialog;
        public RelayCommand ToggleDialog
        {
            get
            {
                return toogleDialog ??
                  (toogleDialog = new RelayCommand(obj =>
                  {
                      this.IsOpen = !this.IsOpen;
                  }));
            }
        }

        private RelayCommand editButton;
        public RelayCommand EditButton
        {
            get
            {
                return editButton ??
                  (editButton = new RelayCommand(obj =>
                  {
                      var service = (SettingBtn)obj;
                      MainWindowModel.startAdmin.ChangeItem(new Setting(service), "Edit");
                  }));
            }
        }

        private RelayCommand openHistory;
        public RelayCommand OpenHistory
        {
            get
            {
                return openHistory ??
                  (openHistory = new RelayCommand(obj =>
                  {
                      MainWindowModel.startAdmin.ChangeItem(new History(), "History");
                  }));
            }
        }

        private SettingBtn _selected;
        public SettingBtn SelectedService
        {
            get { return _selected; }
            set { _selected = value; }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "InputName":
                        if (InputName.Length < 2)
                        {
                            error = "Length of name must be more than 1";
                        }
                        break;

                    case "InputCost":
                        try
                        {
                            Convert.ToDouble(InputCost);
                            if (Convert.ToDouble(InputCost) <= 0)
                            {
                                error = "Cost must be more 0";
                            }
                        }
                        catch (Exception)
                        {
                            error = "Salary must be number";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
