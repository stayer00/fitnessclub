﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using static fitness_club.AddEmployee;

namespace fitness_club.ViewModels
{
    class AddEmployeeModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private List<string> genders;
        private List<TypeEmployeeModel> types;
        private string selectedGender;
        private int selectedType;
        private string inputName;
        private string inputSurname;
        private string inputNumberPhone;
        private DateTime inputBirthday;
        private string inputSnils;
        private float inputBaseSalary;
        private string inputPassport;
        private string inputAddress;
        private string inputItn;
        private string inputRecordNumber;
        private string inputLogin;
        private string inputPassword;
        private int dateStartWorkExpirience;
        private List<int> years;
        private ObservableCollection<ItemActivity> coachActivities;
        private ItemActivity selectedItems;

        public event CreateHandler AddEmployee;
        public bool isOpen;
        public bool isSystemUser;
        IDbCrud crudServ;
        public AddEmployeeModel()
        {
            crudServ = WindowStartAdmin.crudServ;
            genders = new List<string>()
            {
                "Man", "Women"
            };
            inputName = inputSurname = inputNumberPhone = inputSnils = inputPassport = inputAddress = inputItn = inputRecordNumber = inputLogin = inputPassword = "";
            inputBaseSalary = 0;
            inputBirthday = DateTime.Today;
            types = crudServ.GetEmployeeTypes();
            SelectedGender = genders[0];
            selectedType = types[0].ID;
            IsOpen = false;

            coachActivities = new ObservableCollection<ItemActivity>();
            foreach (var service in crudServ.GetServices().Where(i => i.Id !=7 ).ToList())
            {
                coachActivities.Add(new ItemActivity()
                {
                    Item = service.Name,
                    Id = service.Id,
                    IsSelected = false
                });
            }

            years = new List<int>();
            for (int i = 2000; i <= 2020; i++)
            {
                years.Add(i);
            }
            DateStartWorkExpirience = DateTime.Now.Year;

            isSystemUser = false;
        }

        public List<string> Genders
        {
            get
            {
                return this.genders;
            }
        }
        
        public List<int> Years
        {
            get
            {
                return this.years;
            }
        }

        public List<string> Types
        {
            get
            {
                return this.types.Select(i => i.Name).ToList();
            }
        }

        public string SelectedGender
        {
            get
            {
                return this.selectedGender;
            }
            set
            {
                this.selectedGender = value;
                OnPropertyChanged("SelectedGender");
            }
        }

        public int DateStartWorkExpirience
        {
            get
            {
                return this.dateStartWorkExpirience;
            }
            set
            {
                this.dateStartWorkExpirience = value;
                OnPropertyChanged("DateStartWorkExpirience");
            }
        }

        public string SelectedType
        {
            get
            {
                return this.types.Where(i => i.ID == this.selectedType).FirstOrDefault().Name;
            }
            set
            {
                this.selectedType = this.types.Where(i => i.Name == value).FirstOrDefault().ID;
                OnPropertyChanged("SelectedType");
            }
        }

        public string InputName
        {
            get
            {
                return this.inputName;
            }
            set
            {
                this.inputName = value;
                OnPropertyChanged("InputName");
            }
        }

        public string InputSurname
        {
            get
            {
                return this.inputSurname;
            }
            set
            {
                this.inputSurname = value;
                OnPropertyChanged("InputSurname");
            }
        }

        public string InputNumberPhone
        {
            get
            {
                return this.inputNumberPhone;
            }
            set
            {
                this.inputNumberPhone = value;
                OnPropertyChanged("InputNumberPhone");
            }
        }

        public string InputSnils
        {
            get
            {
                return this.inputSnils;
            }
            set
            {
                this.inputSnils = value;
                OnPropertyChanged("InputSnils");
            }
        }

        public float InputBaseSalary
        {
            get
            {
                return this.inputBaseSalary;
            }
            set
            {
                this.inputBaseSalary = value;
                OnPropertyChanged("InputBaseSalary");
            }
        }

        public string InputPassport
        {
            get
            {
                return this.inputPassport;
            }
            set
            {
                this.inputPassport = value;
                OnPropertyChanged("InputPassport");
            }
        }

        public string InputAddress
        {
            get
            {
                return this.inputAddress;
            }
            set
            {
                this.inputAddress = value;
                OnPropertyChanged("InputAddress");
            }
        }

        public string InputItn
        {
            get
            {
                return this.inputItn;
            }
            set
            {
                this.inputItn = value;
                OnPropertyChanged("InputItn");
            }
        }

        public string InputRecordNumber
        {
            get
            {
                return this.inputRecordNumber;
            }
            set
            {
                this.inputRecordNumber = value;
                OnPropertyChanged("InputRecordNumber");
            }
        }

        public DateTime InputBirthday
        {
            get
            {
                return this.inputBirthday;
            }
            set
            {
                this.inputBirthday = value;
                OnPropertyChanged("InputBirthday");
            }
        }

        public string InputLogin
        {
            get
            {
                return this.inputLogin;
            }
            set
            {
                this.inputLogin = value;
                OnPropertyChanged("InputLogin");
            }
        }

        public string InputPassword
        {
            get
            {
                return this.inputPassword;
            }
            set
            {
                this.inputPassword = value;
                OnPropertyChanged("InputPassword");
            }
        }

        public bool IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }

        public bool IsSystemUser
        {
            get
            {
                return this.isSystemUser;
            }
            set
            {
                this.isSystemUser = value;
                OnPropertyChanged("IsSystemUser");
            }
        }


        public ObservableCollection<ItemActivity> Services
        {
            get
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"ListBox {DateTime.Now} get services");
                }
                return coachActivities;
            }
            set
            {
                coachActivities = value;
                OnPropertyChanged("SelectedItems");
            }
        }

        public ItemActivity SelectedItems
        {
            get
            {
                return selectedItems;
            }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"ListBox_item {DateTime.Now} click service_employee");
                }
                var selectedItems = Services.Where(x => x.IsSelected).Count();
                OnPropertyChanged("SelectedItems");
            }
        }
        

        private RelayCommand createEmployee;
        public RelayCommand CreateEmployee
        {
            get
            {
                return createEmployee ??
                  (createEmployee = new RelayCommand(obj =>
                  {

                      var employee = new BLL.EmployeeModel()
                      {
                          Name = inputName,
                          Surname = inputSurname,
                          Birthday = inputBirthday,
                          Gender = selectedGender,
                          NumberPhone = inputNumberPhone,
                          Snils = inputSnils,
                          EmploymentRecordNumber = inputRecordNumber,
                          TypeEmployeeID = selectedType,
                          BaseSalary = inputBaseSalary,
                          ITN = inputItn,
                          Address = inputAddress,
                          Passport = inputPassport,
                          GetStarted = DateTime.Today
                      };

                      if (isSystemUser == false)
                      {
                          inputLogin = "";
                          inputPassword = "";
                      }
                      
                      this.IsOpen = false;
                      AddEmployee(crudServ.CreateEmployee(employee, 
                            new DateTime(DateStartWorkExpirience, 1,1), coachActivities.Where(i => i.IsSelected == true).ToList().Select(i => new ServiceModel() { Id = i.Id }).ToList(),
                            inputLogin, inputPassword));
                  },
                  (obj) => (InputName.Length >= 2 && InputSurname.Length >= 2 && InputNumberPhone.Length == 11 && InputBirthday <= DateTime.Now.AddYears(-18) &&
                  InputSnils.Length == 14 && InputPassport.Length == 11 && ((selectedType == 1 && DateStartWorkExpirience <= DateTime.Now.AddYears(-1).Year) || selectedType != 1) && InputAddress.Length >= 4
                  && InputRecordNumber.Length >= 5 && InputItn.Length >= 5 && ((isSystemUser == true && inputLogin.Length >= 5 && inputPassword.Length >= 5) || isSystemUser == false))));
            }
        }

        private RelayCommand toogleDialog;
        public RelayCommand ToggleDialog
        {
            get
            {
                return toogleDialog ??
                  (toogleDialog = new RelayCommand(obj =>
                  {
                      using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                      {
                          sw.WriteLine($"Button {DateTime.Now} open/close modal_window");
                      }
                      this.IsOpen = !this.IsOpen;
                  }));
            }
        }


        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "InputName":
                        if (InputName.Length < 2)
                        {
                            error = "Length of name must be more than 1";
                        }
                        break;
                    case "InputSurname":
                        if (InputSurname.Length < 2)
                        {
                            error = "Length of surname must be more than 1";
                        }
                        break;
                    case "InputNumberPhone":
                        if (InputNumberPhone.Length != 11)
                        {
                            error = "Length of number phone must be equals 11";
                        }
                        break;
                    case "InputBirthday":
                        if (InputBirthday > DateTime.Now.AddYears(-18))
                        {
                            error = "Invalid date";
                        }
                        break;

                    case "InputSnils":
                        if (InputSnils.Length != 14)
                        {
                            error = "Length of number phone must be equals 14";
                        }
                        break;

                    case "InputBaseSalary":
                        try
                        {
                            Convert.ToDouble(InputBaseSalary);
                            if (Convert.ToDouble(InputBaseSalary) <= 0)
                            {
                                error = "Salary must be more 0";
                            }
                        }
                        catch (Exception)
                        {
                            error = "Salary must be number";
                        }
                        break;

                    case "InputPassport":
                        if (InputPassport.Length != 11)
                        {
                            error = "Length of passport must be equals 11";
                        }
                        break;

                    case "DateStartWorkExpirience":
                        if (DateStartWorkExpirience > DateTime.Now.AddYears(-1).Year)
                        {
                            error = "Experience must be more than 1 year";
                        }
                        break;

                    case "InputAddress":
                        if (InputAddress.Length < 4)
                        {
                            error = "Length of address must be more 3";
                        }
                        break;

                    case "InputRecordNumber":
                        if (InputRecordNumber.Length < 5)
                        {
                            error = "Length of employment record number must be more 5";
                        }
                        break;

                    case "InputItn":
                        if (InputItn.Length < 5)
                        {
                            error = "Length of itn must be more 5";
                        }
                        break;

                    case "InputLogin":
                        if (InputLogin.Length < 5)
                        {
                            error = "Length of login must be more 5";
                        }
                        break;

                    case "InputPassword":
                        if (InputPassword.Length < 5)
                        {
                            error = "Length of password must be more 5";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public class ItemActivity: ServiceModel, INotifyPropertyChanged
    {
        private string item;

        public string Item
        {
            get { return item; }
            set
            {
                item = value;
                OnPropertyChanged("Item");
            }
        }

        private bool isSelected;

        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                isSelected = value;
                OnPropertyChanged("IsSelected");
            }
        }
        
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
