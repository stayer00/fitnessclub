﻿using BLL;
using BLL.Interfaces;
using BLL.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    public class DetailEmployeeModel : INotifyPropertyChanged, IDataErrorInfo
    {
        private EmployeeModel employee;
        IDbCrud crudServ;

        private ObservableCollection<ItemActivity> activities;
        private ItemActivity selectedActivity;
        private List<ServiceModel> services;
        private CoachModel coach;

        private List<string> genders;
        private string name;
        private string surname;
        private DateTime birthday;
        private string numberPhone;
        private string gender;

        DateTime getStarted;
        DateTime getStartedWorkExpirience;
        string address;
        float baseSalary;
        string ITN;
        string passport;
        string snils;
        string employmentRecordNumber;

        public DetailEmployeeModel(BLL.EmployeeModel employee)
        {
            isEdit = false;
            this.employee = employee;
            this.Name = employee.Name;
            this.Surname = employee.Surname;
            this.Birthday = employee.Birthday;
            this.NumberPhone = employee.NumberPhone;
            this.Gender = employee.Gender;

            this.GetStarted = employee.GetStarted;
            this.address = employee.Address;
            this.baseSalary = employee.BaseSalary;
            this.ITN = employee.ITN;
            this.passport = employee.Passport;
            this.snils = employee.Snils;
            this.employmentRecordNumber = employee.EmploymentRecordNumber;
            
            crudServ = WindowStartAdmin.crudServ;

            activities = new ObservableCollection<ItemActivity>();
            services = new List<ServiceModel>();
            services = crudServ.GetServices().Where(i => i.Id != 7).ToList();

            coach = new CoachModel();

            foreach (var service in services)
            {
                activities.Add(new ItemActivity
                {
                    Id = service.Id,
                    Item = service.Name,
                    Cost = 0,
                    IsSelected = false
                });
            }

            if (employee.TypeEmployeeID == 1)
            {
                coach = crudServ.GetCoach(employee.ID);
                foreach (var activity in coach.Activities)
                {
                    var act = activities.Where(i => i.Id == activity.idService).FirstOrDefault();
                    var idx = activities.IndexOf(act);
                    if (idx != -1)
                    {
                        activities[idx].IsSelected = true;
                    }
                }
            }

            selectedActivity = null;
            genders = new List<string>()
            {
                "Man", "Women"
            };
        }

        public EmployeeModel Employee
        {
            get { return employee; }
            set
            {
                employee = value;
                OnPropertyChanged("Employee");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public DateTime Birthday
        {
            get { return birthday; }
            set
            {
                birthday = value;
                OnPropertyChanged("Birthday");
            }
        }

        public string NumberPhone
        {
            get { return numberPhone; }
            set
            {
                numberPhone = value;
                OnPropertyChanged("NumberPhone");
            }
        }

        public string Gender
        {
            get { return gender; }
            set
            {
                gender = value;
                OnPropertyChanged("Gender");
            }
        }

        public List<string> Genders
        {
            get { return genders; }
            set
            {
            }
        }
        public int GetGender
        {
            get { return genders.IndexOf(employee.Gender.Trim()); }
            set
            {
                gender = genders[value];
                OnPropertyChanged("GetGender");
            }
        }

        private bool isEdit;
        public bool IsEdit
        {
            get { return isEdit; }
            set
            {
                isEdit = value;
                OnPropertyChanged("IsEdit");
            }
        }

        public bool IsCoach
        {
            get { return employee.TypeEmployeeID == 1; }
            set
            {
                OnPropertyChanged("IsCoach");
            }
        }

        public DateTime GetStarted
        {
            get { return getStarted; }
            set
            {
                getStarted = value;
                OnPropertyChanged("GetStarted");
            }
        }

        public DateTime GetStartedWorkExpirience
        {
            get { return getStarted; }
            set
            {
                getStarted = value;
                OnPropertyChanged("GetStarted");
            }
        }

        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged("Address");
            }
        }

        public float BaseSalary
        {
            get { return baseSalary; }
            set
            {
                baseSalary = value;
                OnPropertyChanged("BaseSalary");
            }
        }

        public string Passport
        {
            get { return passport; }
            set
            {
                passport = value;
                OnPropertyChanged("Passport");
            }
        }

        public string Snils
        {
            get { return snils; }
            set
            {
                snils = value;
                OnPropertyChanged("Snils");
            }
        }

        public string EmploymentRecordNumber
        {
            get { return employmentRecordNumber; }
            set
            {
                employmentRecordNumber = value;
                OnPropertyChanged("EmploymentRecordNumber");
            }
        }

        public string ITNstr
        {
            get { return ITN; }
            set
            {
                ITN = value;
                OnPropertyChanged("ITNstr");
            }
        }
        public ObservableCollection<ItemActivity> Activities
        {
            get { return activities; }
            set
            {
                activities = value;
                OnPropertyChanged("Activities");
            }
        }
        public ItemActivity SelectedItems
        {
            get
            {
                return selectedActivity;
            }
            set
            {
                var selectedItems = activities.Where(x => x.IsSelected).Count();
                OnPropertyChanged("SelectedItems");
            }
        }

        public List<ServiceModel> Services
        {
            get { return services; }
            set
            {
                OnPropertyChanged("Services");
            }
        }

        private RelayCommand returnPrev;
        public RelayCommand ReturnPrev
        {
            get
            {
                return returnPrev ??
                  (returnPrev = new RelayCommand(obj =>
                  {
                      MainWindowModel.startAdmin.ChangeItem(new Employees(), "Employees");
                  }));
            }
        }

        private RelayCommand saveUpdate;
        public RelayCommand SaveUpdate
        {
            get
            {
                return saveUpdate ??
                  (saveUpdate = new RelayCommand(obj =>
                  {
                      var employee = new BLL.EmployeeModel()
                      {
                          ID = this.employee.ID,
                          Name = name,
                          Surname = surname,
                          Birthday = birthday,
                          Gender = gender,
                          NumberPhone = numberPhone,
                          Snils = snils,
                          EmploymentRecordNumber = employmentRecordNumber,
                          BaseSalary = baseSalary,
                          ITN = ITN,
                          Address = address,
                          Passport = passport,
                          TypeEmployeeID = this.employee.TypeEmployeeID
                      };
                      crudServ.UpdateEmployee(employee, activities.Where(i => i.IsSelected == true).ToList().Select(i => new ServiceModel() { Id = i.Id }).ToList());
                      IsEdit = !IsEdit;
                  },
                  (obj) => (Name.Length >= 2 && Surname.Length >= 2 && NumberPhone.Length == 11 && Birthday <= DateTime.Now.AddYears(-18) &&
                  Snils.Length == 14 && Passport.Length == 11 && Address.Length >= 4 && EmploymentRecordNumber.Length >= 5 && ITN.Length >= 5)));
            }
        }

        private RelayCommand ocEdit;
        public RelayCommand OCEdit
        {
            get
            {
                return ocEdit ??
                  (ocEdit = new RelayCommand(obj =>
                  {
                      IsEdit = !IsEdit;
                  }));
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "Name":
                        if (Name.Length < 2)
                        {
                            error = "Length of name must be more than 1";
                        }
                        break;
                    case "Surname":
                        if (Surname.Length < 2)
                        {
                            error = "Length of surname must be more than 1";
                        }
                        break;
                    case "NumberPhone":
                        if (NumberPhone.Length != 11)
                        {
                            error = "Length of number phone must be equals 11";
                        }
                        break;
                    case "Birthday":
                        if (Birthday > DateTime.Now.AddYears(-18))
                        {
                            error = "Invalid date";
                        }
                        break;

                    case "Snils":
                        if (Snils.Length != 14)
                        {
                            error = "Length of number phone must be equals 14";
                        }
                        break;

                    case "BaseSalary":
                        try
                        {
                            Convert.ToDouble(BaseSalary);
                            if (Convert.ToDouble(BaseSalary) <= 0)
                            {
                                error = "Salary must be more 0";
                            }
                        }
                        catch (Exception)
                        {
                            error = "Salary must be number";
                        }
                        break;

                    case "Passport":
                        if (Passport.Length != 11)
                        {
                            error = "Length of passport must be equals 11";
                        }
                        break;


                    case "Address":
                        if (Address.Length < 4)
                        {
                            error = "Length of address must be more 3";
                        }
                        break;

                    case "EmploymentRecordNumber":
                        if (EmploymentRecordNumber.Length < 5)
                        {
                            error = "Length of employment record number must be more 5";
                        }
                        break;

                    case "ITNstr":
                        if (ITNstr.Length < 5)
                        {
                            error = "Length of itn must be more 5";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
