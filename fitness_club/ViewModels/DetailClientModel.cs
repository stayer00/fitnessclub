﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    class DetailClientModel: INotifyPropertyChanged, IDataErrorInfo
    {
        private ClientModel client;
        ISeasonTicketService stServ;
        IDbCrud crudServ;

        private BindingList<SeasonTicketModel> activeST;
        private BindingList<SeasonTicketModel> unactiveST;
        private List<string> genders;

        private string name;
        private string surname;
        private DateTime birthday;
        private string numberPhone;
        private string gender;

        public delegate void AccountHandler();
        public event AccountHandler Update;
        public DetailClientModel(ClientModel client)
        {
            isEdit = false;
            this.client = client;
            this.Name = client.Name;
            this.Surname = client.Surname;
            this.Birthday = client.Birthday;
            this.NumberPhone = client.NumberPhone;
            this.Gender = client.Gender;
            stServ = WindowStartAdmin.stServ;
            crudServ = WindowStartAdmin.crudServ;

            unactiveST = new BindingList<SeasonTicketModel>();
            activeST = new BindingList<SeasonTicketModel>();
            foreach (var st in stServ.GetSeasonTickets(client.ID))
            {
                if (st.Count == st.classesTotal || st.dateEnd < DateTime.Now)
                {
                    unactiveST.Add(st);
                }
                else activeST.Add(st);
            }
            //activeList.ItemsSource = activeST;
            //unactiveList.ItemsSource = unactiveST;

            genders = new List<string>()
            {
                "Man", "Women"
            };
        }

        public ClientModel Client
        {
            get { return client; }
            set
            {
                client = value;
                OnPropertyChanged("Client");
            }
        }

        public string Name
        {
            get { return name; }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"TextBox {DateTime.Now} change name");
                }
                name = value;
                OnPropertyChanged("Name");
            }
        }

        public string Surname
        {
            get { return surname; }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"TextBox {DateTime.Now} change surname");
                }
                surname = value;
                OnPropertyChanged("Surname");
            }
        }

        public DateTime Birthday
        {
            get { return birthday; }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"DatePicker {DateTime.Now} set birthday");
                }
                birthday = value;
                OnPropertyChanged("Birthday");
            }
        }
        
        public string NumberPhone
        {
            get { return numberPhone; }
            set
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"TextBox {DateTime.Now} change number_phone");
                }
                numberPhone = value;
                OnPropertyChanged("NumberPhone");
            }
        }

        public string Gender
        {
            get { return gender; }
            set
            {
                gender = value;
                OnPropertyChanged("Gender");
            }
        }

        public List<string> Genders
        {
            get { return genders; }
            set
            {
            }
        }

        public BindingList<SeasonTicketModel> ActiveST
        {
            get { return activeST; }
            set
            {
                activeST = value;
                OnPropertyChanged("ActiveST");
            }
        }

        public BindingList<SeasonTicketModel> UnactiveST
        {
            get { return unactiveST; }
            set
            {
                unactiveST = value;
                OnPropertyChanged("UnactiveST");
            }
        }

        public int GetGender
        {
            get {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"ComboBox {DateTime.Now} select gender");
                }
                return genders.IndexOf(client.Gender.Trim()); }
            set
            {
                gender = genders[value];
                OnPropertyChanged("GetGender");
            }
        }


        private bool isEdit;
        public bool IsEdit
        {
            get { return isEdit; }
            set
            {
                isEdit = value;
                OnPropertyChanged("IsEdit");
            }
        }

        private RelayCommand returnPrev;
        public RelayCommand ReturnPrev
        {
            get
            {
                return returnPrev ??
                  (returnPrev = new RelayCommand(obj =>
                  {
                      MainWindowModel.startAdmin.ChangeItem(new Clients(), "Clients");
                  }));
            }
        }

        private RelayCommand ocEdit;
        public RelayCommand OCEdit
        {
            get
            {
                return ocEdit ??
                  (ocEdit = new RelayCommand(obj =>
                  {
                      IsEdit = !IsEdit;
                  }));
            }
        }

        private RelayCommand addVisiting;
        public RelayCommand AddVisiting
        {
            get
            {
                return addVisiting ??
                  (addVisiting = new RelayCommand(obj =>
                  {
                      var st = (SeasonTicketModel)obj;
                      stServ.AddVisiting(st.ID);
                      RefreshSeasonTicket(st.ID);
                  }));
            }
        }

        private RelayCommand saveUpdate;
        public RelayCommand SaveUpdate
        {
            get
            {
                return saveUpdate ??
                  (saveUpdate = new RelayCommand(obj =>
                  {
                      var client = new ClientModel()
                      {
                          ID = this.client.ID,
                          Name = Name,
                          Surname = Surname,
                          Birthday = Birthday,
                          Gender = Gender,
                          DateRegistry = this.client.DateRegistry,
                          NumberPhone = NumberPhone
                      };
                      Client = client;
                      this.client = client;
                      crudServ.UpdateClient(client);
                      IsEdit = !IsEdit;
                  },
                  (obj) => (Name.Length > 1 && Surname.Length > 1 && Birthday <= DateTime.Now.AddYears(-5) && NumberPhone.Length == 11)));
            }
        }

        public void AddNewSeasonTicket(SeasonTicketModel seasonTicketModel, bool isNowVisited)
        {
            seasonTicketModel.Service = crudServ.GetService(seasonTicketModel.ServiceID);
            if (seasonTicketModel.CoachID != null)
            {
                seasonTicketModel.Coach = crudServ.GetCoach(Convert.ToInt32(seasonTicketModel.CoachID));
            }
            if (isNowVisited != false && ((seasonTicketModel.dateStart == seasonTicketModel.dateEnd  && seasonTicketModel.dateEnd == DateTime.Today) && seasonTicketModel.classesTotal == 1 && seasonTicketModel.classesLeft == 1))
            {
                UnactiveST.Add(seasonTicketModel);
            }
            else ActiveST.Add(seasonTicketModel);
            //OnPropertyChanged("AllClients");
        }


        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "Name":
                        if (Name.Length < 2)
                        {
                            error = "Length of name must be more than 1";
                        }
                        break;
                    case "Surname":
                        if (Surname.Length < 2)
                        {
                            error = "Length of surname must be more than 1";
                        }
                        break;
                    case "NumberPhone":
                        if (NumberPhone.Length != 11)
                        {
                            error = "Length of number phone must be equals 11";
                        }
                        break;
                    case "Birthday":
                        if (Birthday > DateTime.Now.AddYears(-5))
                        {
                            error = "Invalid date";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        

        

        void RefreshSeasonTicket(int SeasonTicketID)
        {
            var st = activeST.First(i => i.ID == SeasonTicketID);
            if (st.Count + 1 == st.classesTotal)
            {
                activeST.Remove(st);
                unactiveST.Add(st);
            }
            else
            {
                int ind = activeST.IndexOf(st);
                activeST.Remove(st);
                OnPropertyChanged("ActiveST");
                st.Count++;
                st.classesLeft--;
                activeST.Insert(ind, st);
            }
            //OnPropertyChanged("UnactiveST");
            OnPropertyChanged("ActiveST");
        }
    }
}
