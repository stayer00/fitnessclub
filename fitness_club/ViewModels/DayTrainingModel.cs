﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Controls;

namespace fitness_club.ViewModels
{
    class DayTrainingModel : INotifyPropertyChanged, IDataErrorInfo
    {
        ITrainingAppointmentService trServ;
        ISeasonTicketService stServ;
        IDbCrud crudServ;

        private ObservableCollection<TrainingAppointmentModel> trainingAppointments;
        private ObservableCollection<SeasonTicketModel> clientsSelectedTraining;
        private List<SeasonTicketModel> activeSeasonTickets;
        private List<ServiceModel> services;
        private ObservableCollection<CoachModel> coaches;
        private List<LoungeModel> lounges;
        private DateTime date;
        private bool isAdding = false;

        private ServiceModel selectedService;
        private CoachModel selectedCoach;
        private LoungeModel selectedLounge;
        private DateTime selectedTime;
        private int selectedCount;
        private bool isEdit;
        DateTime SelectedDate;
        Timer timer = new Timer(3000);
        public DayTrainingModel(DateTime date)
        {
            timer.Elapsed += OnTimedEvent;

            SelectedDate = date;

            trServ = WindowStartAdmin.trServ;
            stServ = WindowStartAdmin.stServ;
            crudServ = WindowStartAdmin.crudServ;

            this.trainingAppointments = new ObservableCollection<TrainingAppointmentModel>();

            foreach (var item in trServ.GetTrainingAppointments(date.Date))
            {
                this.trainingAppointments.Add(item);
            }

            this.services = crudServ.GetServices();

            this.coaches = new ObservableCollection<CoachModel>();
            foreach (var coach in crudServ.GetCoaches())
            {
                this.coaches.Add(coach);
            };

            this.lounges = crudServ.GetLounges();

            this.clientsSelectedTraining = new ObservableCollection<SeasonTicketModel>();
            this.date = date;
            activeSeasonTickets = new List<SeasonTicketModel>();

            isEdit = false;

            SelectedCoach = coaches.Where(i => i.Surname == "-").FirstOrDefault();
            SelectedLounge = lounges[0];
            SelectedCount = 0;
            SelectedService = services[0];
            SelectedTime = default(DateTime);

            IsEnabledCreate = true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        public List<ServiceModel> Services
        {
            get
            {
                return services;
            }
            set
            {
                services = value;
            }
        }

        public List<LoungeModel> Lounges
        {
            get
            {
                return lounges;
            }
            set
            {
                lounges = value;
            }
        }

        public ObservableCollection<CoachModel> Coaches
        {
            get
            {
                var newCoaches = new ObservableCollection<CoachModel>();
                foreach (var coach in coaches.Where(i => i.Activities.Where(j => j.idService == selectedService.Id).FirstOrDefault() != null).ToList())
                {
                    newCoaches.Add(coach);
                }
                SelectedCoach = newCoaches[0];
                return newCoaches;
            }
            set
            {
                coaches = value;
            }
        }

        public ServiceModel SelectedService
        {
            get { return selectedService; }
            set
            {
                selectedService = value;

                OnPropertyChanged("SelectedService");
                OnPropertyChanged("Coaches");
            }
        }

        public CoachModel SelectedCoach
        {
            get { return selectedCoach; }
            set
            {
                selectedCoach = value;
                OnPropertyChanged("SelectedCoach");
            }
        }

        public LoungeModel SelectedLounge
        {
            get { return selectedLounge; }
            set
            {
                selectedLounge = value;
                OnPropertyChanged("SelectedLounge");
            }
        }

        public DateTime SelectedTime
        {
            get { return selectedTime; }
            set
            {
                selectedTime = value;
                OnPropertyChanged("SelectedTime");
            }
        }

        public int SelectedCount
        {
            get { return selectedCount; }
            set
            {
                selectedCount = value;
                OnPropertyChanged("SelectedCount");
            }
        }

        public bool isEnabled
        {
            get
            {
                return SelectedTraining != null;
            }
            set
            {
                isEnabled = value;
                //OnPropertyChanged("isEnabled");
            }
        }

        private TrainingAppointmentModel _selected;
        public TrainingAppointmentModel SelectedTraining
        {
            get { return _selected; }
            set
            {
                if (!isAdding)
                {
                    _selected = value;
                    OnPropertyChanged("isEnabled");//isEnabled = _selected != null;
                    OnPropertyChanged("SelectedTraining");
                }
            }
        }

        private SeasonTicketModel selectedClient;
        public SeasonTicketModel SelectedClient
        {
            get { return selectedClient; }
            set
            {
                selectedClient = value;
                OnPropertyChanged("SelectedClient");
            }
        }

        public ObservableCollection<TrainingAppointmentModel> TrainingAppointments
        {
            get { return trainingAppointments; }
            set
            {
                trainingAppointments = value;
                OnPropertyChanged("TrainingAppointments");
            }
        }

        public List<SeasonTicketModel> ActiveSeasonTickets
        {
            get { return activeSeasonTickets; }
            set
            {
                activeSeasonTickets = value;
                OnPropertyChanged("ActiveSeasonTickets");
            }
        }

        public ObservableCollection<SeasonTicketModel> ClientsSelectedTraining
        {
            get { return clientsSelectedTraining; }
            set
            {
                clientsSelectedTraining = value;
                OnPropertyChanged("ClientsSelectedTraining");
            }
        }

        public DateTime DateDay
        {
            get { return date; }
            set
            {
                date = value;
                OnPropertyChanged("DateDay");
            }
        }

        private RelayCommand changeSelectedTraining;
        public RelayCommand ChangeSelectedTraining
        {
            get
            {
                return changeSelectedTraining ??
                  (changeSelectedTraining = new RelayCommand(obj =>
                  {
                      if (isEdit == false)
                      {
                        this.SelectedTraining = (TrainingAppointmentModel)obj;
                      }

                      ClientsSelectedTraining.Clear();

                      if (this.SelectedTraining != null)
                      {
                          if (this.SelectedTraining.TrainingClients != null)
                          {
                              foreach (var item in this.SelectedTraining.TrainingClients)
                              {
                                  ClientsSelectedTraining.Add(item);
                              }
                          }
                          ActiveSeasonTickets = stServ.GetActiveSeasonTicket(SelectedTraining.ServiceID);
                      }
                  }));
            }
        }

        private RelayCommand removeAppointment;
        public RelayCommand RemoveAppointment
        {
            get
            {
                return removeAppointment ??
                  (removeAppointment = new RelayCommand(obj =>
                  {
                      var trainingAppointment = (TrainingAppointmentModel)obj;
                      //trServ.DeleteTrainingAppointment(trainingAppointment.Id);
                      //trainingAppointments.Remove(trainingAppointment);
                      //clientsSelectedTraining.Clear();
                      var ind = trainingAppointments.IndexOf(trainingAppointment);
                      TrainingAppointments.RemoveAt(ind);
                      trainingAppointment.IsCanceled = true;
                      trServ.ToggleCancelTrA(trainingAppointment.Id);
                      TrainingAppointments.Insert(ind, trainingAppointment);
                  }));
            }
        }

        private RelayCommand pointVisiting;
        public RelayCommand PointVisiting
        {
            get
            {
                return pointVisiting ??
                  (pointVisiting = new RelayCommand(obj =>
                  {
                      var client = (SeasonTicketModel)obj;
                      stServ.AddVisiting(client.ID);
                  }));
            }
        }

        private RelayCommand removeClient;
        public RelayCommand RemoveClient
        {
            get
            {
                return removeClient ??
                  (removeClient = new RelayCommand(obj =>
                  {
                      isEdit = true;
                      var client = (SeasonTicketModel)obj;
                      trServ.DeleteClientTrainingAppointment(SelectedTraining.Id, client.ID);
                      SelectedTraining.TrainingClients.Remove(client);

                      ClientsSelectedTraining.Remove(client);

                      int ind = trainingAppointments.IndexOf(SelectedTraining);
                      var tmp = SelectedTraining;

                      trainingAppointments.Remove(SelectedTraining);
                      tmp.Count--;
                      SelectedTraining = tmp;

                      foreach (var item in SelectedTraining.TrainingClients)
                      {
                          ClientsSelectedTraining.Add(item);
                      }

                      trainingAppointments.Insert(ind, SelectedTraining);

                      isEdit = false;
                      //activeSeasonTickets.Remove(activeSeasonTickets.Where(i => i.ClientID == client.ID).FirstOrDefault());
                  }));
            }
        }

        private RelayCommand returnPrev;
        public RelayCommand ReturnPrev
        {
            get
            {
                return returnPrev ??
                  (returnPrev = new RelayCommand(obj =>
                  {
                      UserControl usc = null;
                      MainWindowModel.startAdmin.ChangeItem(new Calendar(), "Schedule");
                  }));
            }
        }

        private RelayCommand addClientToTraining;
        public RelayCommand AddClientToTraining
        {
            get
            {
                return addClientToTraining ??
                  (addClientToTraining = new RelayCommand(obj =>
                  {

                      if (SelectedTraining.MaxCount > SelectedTraining.Count)
                      {
                          isEdit = true;
                          trServ.AddClientToTraining(selectedClient.ID, SelectedTraining.Id);
                          ClientsSelectedTraining.Add(selectedClient);

                          if (SelectedTraining.TrainingClients == null)
                          {
                              SelectedTraining.TrainingClients = new List<SeasonTicketModel>();
                          }
                          SelectedTraining.TrainingClients.Add(selectedClient);
                          int ind = trainingAppointments.IndexOf(SelectedTraining);
                          var tmp = SelectedTraining;
                          trainingAppointments.Remove(SelectedTraining);
                          tmp.Count++;
                          SelectedTraining = tmp;
                          foreach (var item in SelectedTraining.TrainingClients)
                          {
                              ClientsSelectedTraining.Add(item);
                          }
                          trainingAppointments.Insert(ind, SelectedTraining);
                          SelectedClient = null;

                          isEdit = false;

                      }
                  },
                  (obj) =>
                    SelectedTraining != null && selectedClient != null && SelectedTraining.MaxCount > SelectedTraining.Count
                    ));
            }
        }

        private bool isEnabledCreate;
        public bool IsEnabledCreate
        {
            get
            {
                return isEnabledCreate;
            }
            set
            {
                isEnabledCreate = value;
                OnPropertyChanged("IsEnabledCreate");
            }
        }

        private int countLounge
        {
            get
            {
                return lounges.Where(i => i.Id == selectedLounge.Id).FirstOrDefault().MaxCount;
            }
        }

        private RelayCommand addTrainingAppointment;
        public RelayCommand AddTrainingAppointment
        {
            get
            {
                return addTrainingAppointment ??
                  (addTrainingAppointment = new RelayCommand(obj =>
                  {
                      var appointment = new TrainingAppointmentModel();
                      appointment.CoachID = selectedCoach.Id;
                      appointment.LoungeID = selectedLounge.Id;
                      appointment.MaxCount = selectedCount;
                      appointment.ServiceID = selectedService.Id;
                      appointment.Date = new DateTime(date.Year, date.Month, date.Day, selectedTime.Hour, selectedTime.Minute, selectedTime.Second);
                      //appointment.Coach = dbo.GetServiceItem(appointment.CoachID);// this.Coaches.Where(i => i.Id == appointment.CoachID).FirstOrDefault();
                      //appointment.Lounge = this.Lounges.Where(i => i.Id == appointment.LoungeID).FirstOrDefault();
                      //appointment.Service = this.Services.Where(i => i.Id == appointment.ServiceID).FirstOrDefault();

                      if (trServ.CheckCanCreateTraininingAppointment(appointment))
                      {
                          trServ.CreateTrainingAppointment(appointment);
                          appointment.Id = trServ.GetLastTrainingAppointment().Id;


                          trainingAppointments.Add(trServ.GetLastTrainingAppointment());

                          SelectedCoach = Coaches.Where(i => i.Surname == "-").FirstOrDefault();
                          SelectedLounge = Lounges[0];
                          SelectedCount = 0;
                          SelectedService = Services[0];
                          SelectedTime = default(DateTime);
                      }
                      else
                      {
                          IsEnabledCreate = false;

                          timer.Start();
                      }
                  },
                  (obj) => SelectedDate.Date >= DateTime.Today && (SelectedCoach.Surname != "-") && SelectedCount > 0 && countLounge >= SelectedCount &&
                  ((SelectedDate.Date != DateTime.Today && SelectedTime.Hour >= 9 && SelectedTime.Hour <= 22) || (SelectedDate.Date == DateTime.Today && SelectedTime.Hour >= DateTime.Now.Hour && SelectedTime.Hour <= 22))));
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "SelectedCoach":
                        if (SelectedCoach == null)
                        {
                            error = "You should appoint a coach";
                        }
                        else
                        {
                            if (SelectedCoach.Surname == "-")
                            {
                                error = "You should appoint a coach";
                            }

                        }
                        break;
                    case "SelectedTime":
                        if (SelectedTime.Hour < 9 || SelectedTime.Hour > 22)
                        {
                            error = "Time must be in range 9 - 22";
                        }
                        break;
                    case "SelectedCount":
                        if (SelectedCount <= 0)
                        {
                            error = "Count of clients must be more 0";
                        }
                        if (countLounge < selectedCount)
                        {
                            error = "Max count of clients must be less than max count lounge";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            IsEnabledCreate = true;
            timer.Stop();
        }
    }
}
