﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using BLL;
using BLL.Interfaces;
using BLL.Models;
using LiveCharts;
using LiveCharts.Wpf;

namespace fitness_club.ViewModels
{
    class BasicLineModel : INotifyPropertyChanged
    {
        public SeriesCollection SeriesCollection { get; set; }
        public Func<string, string> yFornatter { get; set; }
        public string[] Labels { get; set; }
        
        private Dictionary<string, List<float>> reports;
        IReportService reportServ;
        int selectedYear;
        public BasicLineModel()
        {
            SeriesCollection = new SeriesCollection();
            reportServ = WindowStartAdmin.reportServ;
            //SeriesCollection.Add(new LineSeries
            //{
            //    Title = "Test",
            //    Values = new ChartValues<LineServiceReport> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 }
            //});
            string[] months = new[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            Labels = months;
            yFornatter = value => value.ToString();

            selectedYear = DateTime.Now.Year;

            RefreshCharts();
        }

        public void SignalEventChangingYear(int year)
        {
            selectedYear = year;
            RefreshCharts();
        }

        void RefreshCharts()
        {
            SeriesCollection.Clear();
            reports = reportServ.ReportByServices(selectedYear);

            foreach (var service in reports)
            {
                SeriesCollection.Add(new LineSeries
                {
                    Title = service.Key,
                    Values = new ChartValues<float>(service.Value)
                });
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
