﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    class CalendarModel: INotifyPropertyChanged
    {
        public ObservableCollection<string> DayNames { get; set; }
        public ObservableCollection<Day> Days { get; set; }
        List<string> months;
        List<int> years;
        string selectedMonth;
        int selectedYear;

        public CalendarModel()
        {
            DayNames = new ObservableCollection<string> { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
            Days = new ObservableCollection<Day>();
            months = new List<string> { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            years = new List<int>();

            for (int i = -50; i < 50; i++)
            {
                years.Add(DateTime.Today.AddYears(i).Year);
            }

            selectedMonth = months[DateTime.Today.Month - 1];
            selectedYear = DateTime.Today.Year;
            BuildCalendar(DateTime.Today);
        }

        public List<string> Months
        {
            get
            {
                return months;
            }
        }

        public List<int> Years
        {
            get
            {
                return years;
            }
        }

        public string SelectedMonth
        {
            get
            {
                return selectedMonth;
            }
            set
            {
                selectedMonth = value;
                OnPropertyChanged("SelectedMonth");
                RefreshCalendar();
            }
        }

        public int SelectedYear
        {
            get
            {
                return selectedYear;
            }
            set
            {
                selectedYear = value;
                OnPropertyChanged("SelectedYear");
                RefreshCalendar();
            }
        }
        private void RefreshCalendar()
        {
            int year = (int)selectedYear;

            int month = months.FindIndex(i => i == selectedMonth) + 1;

            DateTime targetDate = new DateTime(year, month, 1);

            BuildCalendar(targetDate);
        }

        public void BuildCalendar(DateTime targetDate)
        {
            Days.Clear();

            //Calculate when the first day of the month is and work out an 
            //offset so we can fill in any boxes before that.
            DateTime d = new DateTime(targetDate.Year, targetDate.Month, 1);
            int offset = DayOfWeekNumber(d.DayOfWeek);
            if (offset != 1) d = d.AddDays(-offset);

            //Show 6 weeks each with 7 days = 42
            for (int box = 1; box <= 42; box++)
            {
                Day day = new Day { Date = d, Enabled = true, IsTargetMonth = targetDate.Month == d.Month };
                //day.PropertyChanged += Day_Changed;
                day.IsToday = d == DateTime.Today;
                Days.Add(day);
                d = d.AddDays(1);
            }
            OnPropertyChanged("Days");
        }

        private RelayCommand openDay;
        public RelayCommand OpenDay
        {
            get
            {
                return openDay ??
                  (openDay = new RelayCommand(obj =>
                  {
                      Day day = (Day)obj;
                      MainWindowModel.startAdmin.ChangeItem(new DayTraining(day.Date), "Day Training");
                  }));
                //условие, при котором будет доступна команда
                //(obj) => (Phones.Count > 0 && selectedPhone != null)));
            }
        }

        private static int DayOfWeekNumber(DayOfWeek dow)
        {
            return Convert.ToInt32(dow.ToString("D"));
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
