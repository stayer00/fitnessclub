﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static fitness_club.AddSeasonTicket;

namespace fitness_club.ViewModels
{
    public class AddSeasonTicketModel: INotifyPropertyChanged, IDataErrorInfo
    {
        ClientModel client;
        public List<ServiceModel> allServices { get; set; }
        public List<CoachModel> allCoaches { get; set; }

        ServiceModel selectedService;
        CoachModel selectedCoach;
        private bool isOneClass;

        int classesTotal = 1;
        ISeasonTicketService stServ;
        IDbCrud crudServ;
        DateTime dateStart = DateTime.Now.Date;
        DateTime dateEnd = DateTime.Now.Date.AddDays(14);

        bool isOpen;

        public event CreateHandler AddSeasonTicket;

        public AddSeasonTicketModel(ClientModel client)
        {
            crudServ = WindowStartAdmin.crudServ;
            this.client = client;
            stServ = WindowStartAdmin.stServ;
            allServices = crudServ.GetServices();
            allCoaches = crudServ.GetCoaches();

            selectedService = allServices[0] != null ? allServices[0] : null;
            SelectedCoach = AvailableCoaches[0] != null ? AvailableCoaches[0] : null;
        }


        public List<CoachModel> AvailableCoaches
        {
            get
            {
                if (selectedService.Id == 7)
                {
                    return allCoaches.Where(i => i.Surname == "-").ToList();
                }
                return allCoaches.Where(i => i.Activities.Where(j => j.idService == selectedService.Id).Count() > 0).ToList();
            }
            set
            {
            }
        }

        public DateTime DateStart
        {
            get
            {
                return this.dateStart;
            }
            set
            {
                this.dateStart = value;
                OnPropertyChanged("DateStart");
            }
        }

        public DateTime DateEnd
        {
            get
            {
                return this.dateEnd;
            }
            set
            {
                this.dateEnd = value;
                OnPropertyChanged("DateEnd");
            }
        }

        public bool IsOpen
        {
            get
            {
                return this.isOpen;
            }
            set
            {
                this.isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }
        public bool IsOneClass
        {
            get
            {
                return isOneClass;
            }
            set
            {
                isOneClass = value;
                OnPropertyChanged("IsOneClass");
            }
        }

        private bool isNowVisiting = false;
        public bool IsNowVisiting
        {
            get
            {
                return isNowVisiting;
            }
            set
            {
                isNowVisiting = value;
                OnPropertyChanged("IsNowVisiting");
            }
        }

        public int ClassesTotal
        {
            get
            {
                return classesTotal;
            }
            set
            {
                classesTotal = value;
                OnPropertyChanged("ClassesTotal");
            }
        }

        public ServiceModel SelectedService
        {
            get
            {
                //SelectedCoach = AvailableCoaches[0];
                return selectedService;
            }
            set
            {
                selectedService = value;
                SelectedCoach = AvailableCoaches[0];
                OnPropertyChanged("AvailableCoaches");
                OnPropertyChanged("SelectedService");
                OnPropertyChanged("SelectedCoach");
            }
        }

        public CoachModel SelectedCoach
        {
            get
            {
                return selectedCoach;
            }
            set
            {
                selectedCoach = value;
                OnPropertyChanged("SelectedCoach");
                OnPropertyChanged("SelectedService");
            }
        }

        private RelayCommand createSeasonTicket;
        public RelayCommand CreateSeasonTicket
        {
            get
            {
                return createSeasonTicket ??
                  (createSeasonTicket = new RelayCommand(obj =>
                  {
                      if (isOneClass == true && isNowVisiting == true)
                      {
                          DateStart = DateTime.Today;
                          DateEnd = DateTime.Today;
                          ClassesTotal = 1;
                          ClassesTotal = 1;
                      }

                      var seasonTicket = new SeasonTicketModel()
                      {
                          ClientID = this.client.ID,
                          ServiceID = SelectedService.Id,
                          CoachID = SelectedCoach.Id,
                          dateStart = DateStart,
                          dateEnd = DateEnd,
                          classesTotal = ClassesTotal,
                          classesLeft = ClassesTotal,
                      };
                      this.IsOpen = false;


                      var newSt = stServ.AddSeasonTicket(seasonTicket);
                      if (newSt != null)
                      {
                          
                          AddSeasonTicket(newSt, isOneClass == true && isNowVisiting == true);
                      }
                      if (isOneClass == true && isNowVisiting == true)
                      {
                          stServ.AddVisiting(newSt.ID);
                      }
                  },
                  (obj) => (SelectedCoach != null && ((SelectedCoach.Surname == "-" && SelectedService.Id == 7) || (SelectedCoach.Surname != "-" && SelectedService.Id != 7) && ClassesTotal > 0
                  && ((DateStart >= DateTime.Today && DateEnd >= DateStart) || isOneClass == true)))));
            }
        }

        float costSearch = 0;

        public float CostSearch
        {
            get
            {
                return costSearch;
            }
            set
            {
                costSearch = value;
                OnPropertyChanged("CostSearch");
            }
        }

        private RelayCommand getTotalCost;
        public RelayCommand GetTotalCost
        {
            get
            {
                return getTotalCost ??
                  (getTotalCost = new RelayCommand(obj =>
                  {
                      var seasonTicket = new SeasonTicketModel()
                      {
                          ClientID = this.client.ID,
                          ServiceID = SelectedService.Id,
                          CoachID = SelectedCoach.Id,
                          dateStart = DateStart,
                          dateEnd = DateEnd,
                          classesTotal = ClassesTotal,
                          classesLeft = ClassesTotal,
                      };
                      CostSearch = stServ.GetCost(seasonTicket);
                  },
                  (obj) => (SelectedCoach != null && ((SelectedCoach.Surname == "-" && SelectedService.Id == 7) || (SelectedCoach.Surname != "-" && SelectedService.Id != 7) && ClassesTotal > 0
                  && ((DateStart >= DateTime.Today && DateEnd >= DateStart) || isOneClass == true)))));
            }
        }

        public string this[string columnName]
        {
            get
            {
                string error = String.Empty;
                switch (columnName)
                {
                    case "SelectedCoach":
                        if (selectedCoach == null)
                        {
                            error = "You should appoint a coach";
                        }
                        else
                        {
                            if (selectedCoach == null && selectedCoach.Surname == "-" && selectedService.Id != 7)
                            {
                                error = "You should appoint a coach";
                            }

                            if (selectedCoach.Surname != "-" && selectedService.Id == 7)
                            {
                                error = "You should unappoint coach";
                            }
                        }
                        break;
                    case "ClassesTotal":
                        if (ClassesTotal <= 0 && isOneClass != true)
                        {
                            error = "Classes total must be more 0";
                        }
                        break;
                    case "DateStart":
                        if (DateStart < DateTime.Today && isOneClass != true)
                        {
                            error = "Date start must be in future";
                        }
                        break;
                    case "DateEnd":
                        if (DateEnd < DateStart && isOneClass != true)
                        {
                            error = "Date end must be more or equals than Date start";
                        }
                        break;
                }
                return error;
            }
        }
        public string Error
        {
            get { throw new NotImplementedException(); }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
