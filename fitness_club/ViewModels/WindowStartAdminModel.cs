﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace fitness_club.ViewModels
{
    public class WindowStartAdminModel : INotifyPropertyChanged, IRequireViewIdentification
    {
        public delegate void AccountHandler(UserControl control, string text);
        public event AccountHandler ChangeSelectedItem;

        private bool isOpen;
        public WindowStartAdminModel()
        {
            isOpen = false;
            _viewId = Guid.NewGuid();
        }

        private Guid _viewId;
        public Guid ViewID
        {
            get { return _viewId; }
        }

        public bool IsOpen
        {
            get
            {
                return isOpen;
            }
            set
            {
                isOpen = value;
                OnPropertyChanged("IsOpen");
            }
        }
        private RelayCommand openMenu;
        public RelayCommand OpenMenu
        {
            get
            {
                return openMenu ??
                    (openMenu = new RelayCommand(obj =>
                    {
                        IsOpen = !IsOpen;
                    })
                    );
            }
        }


        private RelayCommand selectedItemChangedCommand;
        public RelayCommand SelectedItemChangedCommand
        {
            get
            {
                return selectedItemChangedCommand ??
                    (selectedItemChangedCommand = new RelayCommand(obj =>
                    {
                        ListViewItem item = (ListViewItem)obj;
                        UserControl usc;
                        switch (item.Name)
                        {
                            case "ItemClients":
                                usc = new Clients();
                                ChangeSelectedItem(usc, "Clients");
                                break;
                            case "ItemEmployees":
                                usc = new Employees();
                                ChangeSelectedItem(usc, "Employees");
                                break;
                            case "ItemSchedule":
                                usc = new Calendar();
                                ChangeSelectedItem(usc, "Schedule");
                                break;
                            case "ItemStatistics":
                                usc = new Statictics();
                                ChangeSelectedItem(usc, "Statictics");
                                break;
                            case "ItemServices":
                                usc = new Services();
                                ChangeSelectedItem(usc, "Services");
                                break;
                            default:
                                break;
                        }
                    }));
            }
        }

        private RelayCommand logout;
        public RelayCommand ButtonLogout
        {
            get
            {
                return logout ??
                    (logout = new RelayCommand(obj =>
                    {
                        MainWindow mainWindow = new MainWindow();
                        mainWindow.Show();
                        WindowManager.CloseWindow(ViewID);
                    })
                    );
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
