﻿using BLL;
using BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    class ClientsModel : INotifyPropertyChanged, IRequireViewIdentification
    {
        private ObservableCollection<ClientModel> allClients;
        IDbCrud crudServ;
        public ClientsModel()
        {
            _viewId = Guid.NewGuid();
            sortSurnane = "";
            crudServ = WindowStartAdmin.crudServ;

            allClients = new ObservableCollection<ClientModel>();

            foreach (var item in crudServ.GetClients())
                allClients.Add(item);
            SelectedClient = allClients[0];
        }

        private Guid _viewId;
        public Guid ViewID
        {
            get { return _viewId; }
        }

        private RelayCommand details;
        public RelayCommand Details
        {
            get
            {
                return details ??
                    (details = new RelayCommand(obj =>
                    {
                        using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                        {
                            sw.WriteLine($"Button {DateTime.Now} click DetailsClient");
                        }
                        MainWindowModel.startAdmin.ChangeItem(new DetailClient(SelectedClient), "Details client");
                        //ChangeSelectedItem(new DetailClient(ChangeSelectedItem, SelectedClient), "Details client");
                        WindowManager.CloseWindow(ViewID);
                    }));
            }
        }

        private RelayCommand removeClient;
        public RelayCommand RemoveClient
        {
            get
            {
                return removeClient ??
                    (removeClient = new RelayCommand(obj =>
                    {
                        using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                        {
                            sw.WriteLine($"Button {DateTime.Now} click RemoveClient");
                        }
                        ClientModel client = (ClientModel)obj;
                        crudServ.DeleteClient(client.ID);
                        this.allClients.Remove(client);
                        OnPropertyChanged("AllClients");
                    }));
            }
        }

        public ObservableCollection<ClientModel> AllClients
        {
            get
            {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"DataGrid {DateTime.Now} get AllClients");
                }

                ObservableCollection<ClientModel> sortClients = new ObservableCollection<ClientModel>();
                foreach (var client in allClients.Where(i => i.Surname.ToLower().Contains(sortSurnane.ToLower())).ToList())
                {
                    sortClients.Add(client);
                }
                return sortClients;
            }

            set
            {
                allClients = value;
                OnPropertyChanged("AllClients");
            }
        }

        public void AddNewClient(ClientModel client)
        {
            allClients.Add(client);
            OnPropertyChanged("AllClients");
        }

        private ClientModel _selected;
        public ClientModel SelectedClient
        {
            get { return _selected; }
            set {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"DataGrid_row {DateTime.Now} set row_client");
                }
                _selected = value;
            }
        }

        private string sortSurnane;
        public string SortSurnane
        {
            get { return sortSurnane; }
            set {
                using (StreamWriter sw = File.AppendText(Directory.GetCurrentDirectory() + "\\history.txt"))
                {
                    sw.WriteLine($"TextBox {DateTime.Now} change sortSurnane");
                }
                sortSurnane = value;
                OnPropertyChanged("AllClients");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
