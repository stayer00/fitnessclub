﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    class SettingModel : INotifyPropertyChanged
    {
        private View.SettingBtn settingBtn;
        public SettingModel(View.SettingBtn settingBtn)
        {
            this.settingBtn = settingBtn;
            settingColor = settingBtn.Color;
        }

        public string Name {
            get
            {
                return settingBtn.Name;
            }
            set
            {
                settingBtn.Name = value;
                OnPropertyChanged("Name");
            }
        }
        public Color settingColor;
        public Color SettingColor
        {
            get
            {
                return settingBtn.Color;
            }
            set
            {
                settingBtn.Color = value;
                OnPropertyChanged("SettingColor");
            }
        }

        
        private RelayCommand change;
        public RelayCommand Change
        {
            get
            {
                return change ??
                  (change = new RelayCommand(obj =>
                  {
                      var color = (Color)obj;
                      int c = 0;  
                  }));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
