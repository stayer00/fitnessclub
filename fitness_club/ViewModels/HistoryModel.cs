﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    public class HistoryModel: INotifyPropertyChanged
    {
        List<HistoryRecord> records;
        public HistoryModel()
        {
            records = new List<HistoryRecord>();

            var path = Directory.GetCurrentDirectory();
            try
            {
                string[] lines = System.IO.File.ReadAllLines(path + "\\history.txt");


                foreach (string line in lines)
                {
                    var lineSplit = line.Split(' ');

                    Records.Add(new HistoryRecord()
                    {
                        NameElement = lineSplit[0],
                        Date = lineSplit[1],
                        Time = lineSplit[2]
                    });
                }

            }
            catch (Exception e)
            {
                var error = e;
            }
        }

        public List<HistoryRecord> Records
        {
            get
            {
                return this.records;
            }
            set
            {
                this.records = value;
                OnPropertyChanged("Records");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}

public class HistoryRecord
{
    public string NameElement { get; set; }
    public string Date { get; set; }
    public string Time { get; set; }
}
