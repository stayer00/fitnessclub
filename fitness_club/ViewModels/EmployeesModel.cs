﻿using BLL;
using BLL.Interfaces;
using fitness_club.View;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;
using Syncfusion.Pdf.Grid;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    class EmployeesModel : INotifyPropertyChanged
    {
        private ObservableCollection<EmployeeModel> allEmployees;
        IDbCrud crudServ;
        public EmployeesModel()
        {
            sortSurnane = "";
            _viewId = Guid.NewGuid();
            crudServ = WindowStartAdmin.crudServ;

            allEmployees = new ObservableCollection<EmployeeModel>();

            foreach (var item in crudServ.GetEmployees().Where(i => i.Name != "-").ToList())
                allEmployees.Add(item);
            SelectedEmployee = allEmployees[0];
        }

        private Guid _viewId;
        public Guid ViewID
        {
            get { return _viewId; }
        }

        private RelayCommand details;
        public RelayCommand Details
        {
            get
            {
                return details ??
                    (details = new RelayCommand(obj =>
                    {
                        MainWindowModel.startAdmin.ChangeItem(new DetailsEmployee(SelectedEmployee), "Details client");
                        WindowManager.CloseWindow(ViewID);
                    }));
            }
        }
        
        private RelayCommand removeEmployee;
        public RelayCommand RemoveEmployee
        {
            get
            {
                return removeEmployee ??
                    (removeEmployee = new RelayCommand(obj =>
                    {
                        EmployeeModel employee = (EmployeeModel)obj;
                        crudServ.DeleteEmployee(employee.ID);
                        this.allEmployees.Remove(employee);
                        OnPropertyChanged("AllEmployees");
                    }));
            }
        }

        private RelayCommand deliverSalary;
        public RelayCommand DeliverSalary
        {
            get
            {
                return deliverSalary ??
                    (deliverSalary = new RelayCommand(obj =>
                    {
                        var salaries = crudServ.DeliverSalary();

                        using (PdfDocument document = new PdfDocument())
                        {
                            //Create a new PDF document.
                            PdfDocument doc = new PdfDocument();
                            //Add a page.
                            PdfPage page = doc.Pages.Add();

                            PdfGraphics graphics = page.Graphics;
                            PdfFont font = new PdfStandardFont(PdfFontFamily.Helvetica, 15);
                            //Draw the text
                            graphics.DrawString($"Employee salary report  {DateTime.Today.Year} / {DateTime.Today.Month}", font, PdfBrushes.Black, new PointF(0, 0));

                            //Create a PdfGrid.
                            PdfGrid pdfGrid = new PdfGrid();
                            //Create a DataTable.
                            DataTable dataTable = new DataTable();
                            //Add columns to the DataTable
                            dataTable.Columns.Add("Name");
                            dataTable.Columns.Add("Salary");
                            //Add rows to the DataTable.

                            foreach (var salary in salaries)
                            {
                                dataTable.Rows.Add(new object[] { salary.Surname.ToString(), salary.Salary.ToString() });
                            }

                            //Assign data source.
                            pdfGrid.DataSource = dataTable;
                            //Draw grid to the page of PDF document.
                            pdfGrid.Draw(page, new PointF(10, 50));
                            //Save the document.
                            var name = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString();
                            doc.Save(name + ".pdf");
                            //close the document
                            doc.Close(true);

                            System.Diagnostics.Process.Start(name + ".pdf");

                        }
                    }));
            }
        }

        public ObservableCollection<EmployeeModel> AllEmployees
        {
            get
            {
                ObservableCollection<EmployeeModel> sortEmployees = new ObservableCollection<EmployeeModel>();
                foreach (var employee in allEmployees.Where(i => i.Surname.ToLower().Contains(sortSurnane.ToLower())).ToList())
                {
                    sortEmployees.Add(employee);
                }
                return sortEmployees;
            }
            set
            {
                allEmployees = value;
                OnPropertyChanged("AllEmployees");
            }
        }

        private string sortSurnane;
        public string SortSurnane
        {
            get { return sortSurnane; }
            set
            {
                sortSurnane = value;
                OnPropertyChanged("AllEmployees");
            }
        }

        public void AddNewEmployee(EmployeeModel employee)
        {
            allEmployees.Add(employee);
            OnPropertyChanged("AllEmployees");
        }

        private EmployeeModel _selected;
        public EmployeeModel SelectedEmployee
        {
            get { return _selected; }
            set { _selected = value; }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
