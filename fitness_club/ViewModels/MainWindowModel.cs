﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using System.Windows;
using System.ComponentModel;

namespace fitness_club.ViewModels
{
    class MainWindowModel: IRequireViewIdentification, INotifyPropertyChanged
    {
        string login = "";
        string password = "";
        bool isAuth = true;
        Timer timer = new Timer(3000);
        public MainWindowModel()
        {
            _viewId = Guid.NewGuid();
            timer.Elapsed += OnTimedEvent;
            startAdmin = new WindowStartAdmin();
        }
        public string Login
        {
            get
            {
                return login;
            }
            set
            {
                login = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }
            set
            {
                password = value;
            }
        }
        public static WindowStartAdmin startAdmin;

        private Guid _viewId;
        public Guid ViewID
        {
            get { return _viewId; }
        }

        public bool IsAuth
        {
            get { return isAuth; }
            set { isAuth = value; OnPropertyChanged("IsAuth"); }
        }


        private RelayCommand exLogin;
        public RelayCommand ExLogin
        {
            get
            {
                return exLogin ??
                    (exLogin = new RelayCommand(obj =>
                    {
                        try
                        {
                            //if(WindowStartAdmin.crudServ.Auth(login, password))
                            if(true)
                            {
                                
                                startAdmin.Show();
                                WindowManager.CloseWindow(ViewID);
                            }
                            else
                            {
                                IsAuth = false;
                                timer.Start();
                            }
                        }
                        catch (Exception e)
                        {
                            MessageBox.Show(e.Message);
                        }
                    }));
            }
        }

        
        private void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            isAuth = true;
            timer.Stop();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
