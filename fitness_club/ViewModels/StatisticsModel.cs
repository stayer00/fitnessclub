﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.ViewModels
{
    public class StatisticsModel : INotifyPropertyChanged
    {
        private List<int> years;
        private int selectedYear;

        public delegate void EventChangeYear(int year);
        public event EventChangeYear ChangeYear;

        public StatisticsModel()
        {
            years = new List<int>();
            for (int i = 2015; i < 2025; i++)
            {
                years.Add(i);
            }
            selectedYear = DateTime.Now.Year;
        }

        public List<int> Years
        {
            get
            {
                return this.years;
            }
        }

        public int SelectedYear
        {
            get
            {
                return selectedYear;
            }
            set
            {
                selectedYear = value;
                ChangeYear(selectedYear);
                OnPropertyChanged("SelectedYear");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
