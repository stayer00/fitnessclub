﻿using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static fitness_club.ViewModels.StatisticsModel;

namespace fitness_club.View.BasicLine
{
    /// <summary>
    /// Логика взаимодействия для BasicLine.xaml
    /// </summary>
    public partial class BasicLine : UserControl
    {
        BasicLineModel bsM;
        public BasicLine(StatisticsModel stM)
        {
            InitializeComponent();
            bsM = new BasicLineModel();
            stM.ChangeYear += bsM.SignalEventChangingYear;
            DataContext = bsM;
        }

    }
}
