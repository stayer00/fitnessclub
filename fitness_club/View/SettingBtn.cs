﻿using BLL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.View
{
    public class SettingBtn
    {
        public Color Color { get; set; }
        public Image Image { get; set; }
        public bool OnHover { get; set; }
        public bool Type { get; set; }
        public Color ColorShadow { get; set; }
        public Color ColorOnHover { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public int Cost { get; set; }

        public SettingBtn()
        {

        }

        public SettingBtn(ServiceModel service)
        {
            Id = service.Id;
            Name = service.Name;
            Cost = service.Cost;
            Color = Color.Blue;
            Type = false;
            OnHover = true;
            ColorOnHover = Color.Black;
            ColorShadow = Color.White;
        }
    }
}
