﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для DetailClient.xaml
    /// </summary>
    public partial class DetailClient 
    {
        DetailClientModel dcm;
        public DetailClient(ClientModel client)
        {
            
            InitializeComponent();

            AddSeasonTicket.Children.Clear();

            AddSeasonTicket usc = new AddSeasonTicket(client);

            dcm = new DetailClientModel(client);
            usc.AddNewSeasonTicket += dcm.AddNewSeasonTicket;

            usc.PutEvent();

            AddSeasonTicket.Children.Add(usc);


            this.DataContext = dcm;
        }

    }
}
