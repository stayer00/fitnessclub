﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для Employees.xaml
    /// </summary>
    public partial class Employees : UserControl
    {
        public Employees()
        {
            InitializeComponent();
            AddEmployee.Children.Clear();
            AddEmployee uscAE = new AddEmployee();
            AddEmployee.Children.Add(uscAE);

            EmployeesModel em = new EmployeesModel();
            uscAE.AddNewEmployee += em.AddNewEmployee;
            this.DataContext = em;

            uscAE.PutEvent();
        }
    }
}
