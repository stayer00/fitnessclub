﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Windows;
using System.Windows.Controls;


namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для AddClient.xaml
    /// </summary>
    public partial class AddEmployee : UserControl
    {
        public delegate void CreateHandler(EmployeeModel employee);
        public event CreateHandler AddNewEmployee;

        AddEmployeeModel uscE;
        public AddEmployee()
        {
            InitializeComponent();
            uscE = new AddEmployeeModel();
            DataContext = uscE;
        }

        public void PutEvent()
        {
            uscE.AddEmployee += AddNewEmployee;
        }
    }
}
