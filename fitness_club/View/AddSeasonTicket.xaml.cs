﻿using BLL;
using BLL.Interfaces;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для AddSeasonTicket.xaml
    /// </summary>
    public partial class AddSeasonTicket : UserControl
    {
        public delegate void CreateHandler(SeasonTicketModel seasonTicket, bool isVisited);
        public event CreateHandler AddNewSeasonTicket;

        AddSeasonTicketModel uST;
        public AddSeasonTicket(ClientModel client)
        {
            InitializeComponent();

            uST = new AddSeasonTicketModel(client);
            DataContext = uST;
        }

        public void PutEvent()
        {
            uST.AddSeasonTicket += AddNewSeasonTicket;
        }
    }
}
