﻿using BLL;
using BLL.Interfaces;
using BLL.Util;
using fitness_club.Util;
using fitness_club.ViewModels;
using Ninject;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для WindowStartAdmin.xaml
    /// </summary>
    public partial class WindowStartAdmin : Window
    {
        public static IDbCrud crudServ;
        public static ISeasonTicketService stServ;
        public static ITrainingAppointmentService trServ;
        public static IReportService reportServ;

        public static UserControl Children;
        public WindowStartAdmin()
        {
            InitializeComponent();
            try
            {
                string connection = ConfigurationManager.ConnectionStrings["FitnessClubEntity"].ConnectionString;
                var kernel = new StandardKernel(new NinjectRegistrations(), new ServiceModule(connection));

                crudServ = kernel.Get<IDbCrud>();
                stServ = kernel.Get<ISeasonTicketService>();
                trServ = kernel.Get<ITrainingAppointmentService>();
                reportServ = kernel.Get<IReportService>();
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            var dc = new WindowStartAdminModel();
            dc.ChangeSelectedItem += ChangeItem;
            DataContext = dc;
            ChangeItem(new Clients(), "Clients");
        }

        public void ChangeItem(UserControl usc, string text)
        {
            MainContent.Children.Clear();
            MainContent.Children.Add(usc);
            page.Text = text;
        }
    }
}
