﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для Clients.xaml
    /// </summary>
    public partial class Clients : UserControl
    {
        public Clients()
        {
            InitializeComponent();
            AddClient.Children.Clear();
            AddClient uscACl = new AddClient();
            AddClient.Children.Add(uscACl);

            ClientsModel cla = new ClientsModel();

            uscACl.AddNewClient += cla.AddNewClient;
            this.DataContext = cla;

            uscACl.PutEvent();
        }
    }
}
