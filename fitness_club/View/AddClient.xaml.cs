﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;


namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для AddClient.xaml
    /// </summary>
    public partial class AddClient : UserControl
    {
        public delegate void CreateHandler(ClientModel client);
        public event CreateHandler AddNewClient;

        AddClientModel uscACl;
        public AddClient()
        {
            InitializeComponent();
            uscACl = new AddClientModel();
            DataContext = uscACl;
        }

        public void PutEvent()
        {
            uscACl.AddClient += AddNewClient;
        }
    }
}
