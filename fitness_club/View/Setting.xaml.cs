﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Drawing;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для Setting.xaml
    /// </summary>
    public partial class Setting : UserControl, INotifyPropertyChanged
    {
        private bool check = false;

        public string NameS { get; set; }
        public string Cost { get; set; }
        private string backgrColor { get; set; }
        private string fontColor { get; set; }
        private string colorShadow { get; set; }
        private string backColorOnHover { get; set; }


        public string BackColorOnHover
        {
            get
            {
                return backColorOnHover;
            }
            set
            {
                backColorOnHover = value;
                OnPropertyChanged("BackColorOnHover");
            }
        }
        public string ColorShadow
        {
            get
            {
                return colorShadow;
            }
            set
            {
                colorShadow = value;
                OnPropertyChanged("ColorShadow");
            }
        }
        public string BackColor
        {
            get
            {
                return backgrColor;
            }
            set
            {
                backgrColor = value;
                OnPropertyChanged("BackColor");
            }
        }

        public string FontColor
        {
            get
            {
                return fontColor;
            }
            set
            {
                fontColor = value;
                OnPropertyChanged("FontColor");
            }
        }
        public Setting(View.SettingBtn settingBtn)
        {
            InitializeComponent();
            NameS = settingBtn.Name;
            Cost = settingBtn.Cost.ToString();
            BackColor = "Blue";
            FontColor = "White";
            ColorShadow = "DarkBlue";
            BackColorOnHover = "LightBlue";

            nameServiceImage.Text = settingBtn.Name;
            costImage.Text = settingBtn.Cost.ToString() + " rub";
            DataContext = this;
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Title = "Select a picture";
            op.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" +
              "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
              "Portable Network Graphic (*.png)|*.png";
            if (op.ShowDialog() == true)
            {
                image.ImageSource = new BitmapImage(new Uri(op.FileName));
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (check == true)
            {
                colorPicker.IsEnabled = true;
                btnLoad.IsEnabled = false;

                //service.Visibility = Visibility.Visible;
                serviceImage.Visibility = Visibility.Collapsed;
            }
            check = true;
        }

        private void RadioButton_Checked_1(object sender, RoutedEventArgs e)
        {
            if (check == true)
            {
                colorPicker.IsEnabled = false;
                btnLoad.IsEnabled = true;

                //service.Visibility = Visibility.Collapsed;
                serviceImage.Visibility = Visibility.Visible;
            }
        }

        private void colorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            BackColor = e.NewValue.ToString();
        }
       
        private void hoverColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            BackColorOnHover = e.NewValue.ToString();
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            var handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        private void fontColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            FontColor = e.NewValue.ToString();
        }

        private void shadowColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            ColorShadow = e.NewValue.ToString();
        }
    }
}
