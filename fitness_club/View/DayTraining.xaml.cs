﻿using BLL;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для DayTraining.xaml
    /// </summary>
    public partial class DayTraining
    {
        public DayTraining(DateTime date)
        {
            InitializeComponent();
            this.DataContext = new DayTrainingModel(date);
        }
    }
}
