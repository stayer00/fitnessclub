﻿using fitness_club.View.BasicLine;
using fitness_club.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
//using Wpf.CartesianChart.BasicLine;

namespace fitness_club
{
    /// <summary>
    /// Логика взаимодействия для Statictics.xaml
    /// </summary>
    public partial class Statictics : UserControl
    {
        public Statictics()
        {
            InitializeComponent();
            Chart.Children.Clear();

            var stM = new StatisticsModel();

            var usc = new UserControl();

            usc = new BasicLine(stM);

            DataContext = stM;
            Chart.Children.Add(usc);
        }
    }
}
