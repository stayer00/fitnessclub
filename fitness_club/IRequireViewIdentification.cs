﻿using System;

namespace fitness_club
{
    public interface IRequireViewIdentification
    {
        Guid ViewID { get; }
    }
}