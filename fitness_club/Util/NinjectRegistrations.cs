﻿using BLL;
using BLL.Interfaces;
using BLL.Services;
using Ninject.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fitness_club.Util
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<ISeasonTicketService>().To<SeasonTicketService>();
            Bind<IReportService>().To<ReportService>();
            Bind<IDbCrud>().To<DbDataOperation>();
            Bind<ITrainingAppointmentService>().To<TrainingAppointmentService>();
        }
    }
}
