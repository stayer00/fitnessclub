USE [master]
GO
/****** Object:  Database [FitnessСlub]    Script Date: 25.12.2020 17:26:03 ******/
CREATE DATABASE [FitnessСlub]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'FitnessСlub', FILENAME = N'C:\Users\Vitalii\FitnessСlub.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'FitnessСlub_log', FILENAME = N'C:\Users\Vitalii\FitnessСlub_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [FitnessСlub] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [FitnessСlub].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [FitnessСlub] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [FitnessСlub] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [FitnessСlub] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [FitnessСlub] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [FitnessСlub] SET ARITHABORT OFF 
GO
ALTER DATABASE [FitnessСlub] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [FitnessСlub] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [FitnessСlub] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [FitnessСlub] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [FitnessСlub] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [FitnessСlub] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [FitnessСlub] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [FitnessСlub] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [FitnessСlub] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [FitnessСlub] SET  ENABLE_BROKER 
GO
ALTER DATABASE [FitnessСlub] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [FitnessСlub] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [FitnessСlub] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [FitnessСlub] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [FitnessСlub] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [FitnessСlub] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [FitnessСlub] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [FitnessСlub] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [FitnessСlub] SET  MULTI_USER 
GO
ALTER DATABASE [FitnessСlub] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [FitnessСlub] SET DB_CHAINING OFF 
GO
ALTER DATABASE [FitnessСlub] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [FitnessСlub] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [FitnessСlub] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [FitnessСlub] SET QUERY_STORE = OFF
GO
USE [FitnessСlub]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
USE [FitnessСlub]
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Activity]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Activity](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CoachID] [int] NOT NULL,
	[ServiceID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Activity] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AppointmentClientTrainings]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AppointmentClientTrainings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TrainingAppointmentID] [int] NOT NULL,
	[SeasonTicketID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AppointmentClientTrainings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[ID] [int] NOT NULL,
	[dateRegistry] [date] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Client] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Coaches]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Coaches](
	[ID] [int] NOT NULL,
	[dateStartWorkExpirience] [datetime] NOT NULL,
 CONSTRAINT [PK_dbo.Coaches] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[ID] [int] NOT NULL,
	[address] [nvarchar](max) NOT NULL,
	[getStarted] [date] NOT NULL,
	[baseSalary] [real] NOT NULL,
	[employmentRecordNumber] [nvarchar](max) NOT NULL,
	[ITN] [nvarchar](max) NOT NULL,
	[passport] [nvarchar](max) NOT NULL,
	[snils] [nvarchar](max) NOT NULL,
	[TypeEmployeeID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Employee] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Human]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Human](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[surname] [nvarchar](max) NOT NULL,
	[birthday] [date] NOT NULL,
	[gender] [nchar](10) NOT NULL,
	[numberPhone] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_dbo.Human] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Lounge]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lounge](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[maxCount] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Lounge] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salary]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[year] [int] NOT NULL,
	[month] [int] NOT NULL,
	[salary] [real] NOT NULL,
	[EmployeeID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Salary] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SeasonTicket]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SeasonTicket](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ServiceID] [int] NOT NULL,
	[ClientID] [int] NOT NULL,
	[CoachID] [int] NULL,
	[classesTotal] [int] NOT NULL,
	[classesLeft] [int] NOT NULL,
	[dateStart] [date] NOT NULL,
	[dateEnd] [date] NOT NULL,
	[saleClasses] [int] NOT NULL,
	[saleConstantClient] [int] NOT NULL,
	[saleAge] [int] NOT NULL,
	[totalCost] [real] NOT NULL,
 CONSTRAINT [PK_dbo.SeasonTicket] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Services]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NULL,
	[cost] [int] NOT NULL,
	[isDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.Services] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TrainingAppointments]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TrainingAppointments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[maxCount] [int] NOT NULL,
	[ServiceID] [int] NOT NULL,
	[CoachID] [int] NOT NULL,
	[LoungeID] [int] NOT NULL,
	[IsCanceled] [bit] NOT NULL,
 CONSTRAINT [PK_dbo.TrainingAppointments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TypeEmployees]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TypeEmployees](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.TypeEmployees] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserSystem]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserSystem](
	[ID] [int] NOT NULL,
	[email] [nvarchar](max) NOT NULL,
	[password] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.UserSystem] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Visitings]    Script Date: 25.12.2020 17:26:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Visitings](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[date] [datetime] NOT NULL,
	[SeasonTicketID] [int] NOT NULL,
 CONSTRAINT [PK_dbo.Visitings] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_CoachID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_CoachID] ON [dbo].[Activity]
(
	[CoachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ServiceID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ServiceID] ON [dbo].[Activity]
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SeasonTicketID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_SeasonTicketID] ON [dbo].[AppointmentClientTrainings]
(
	[SeasonTicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TrainingAppointmentID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_TrainingAppointmentID] ON [dbo].[AppointmentClientTrainings]
(
	[TrainingAppointmentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ID] ON [dbo].[Client]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ID] ON [dbo].[Coaches]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ID] ON [dbo].[Employee]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_TypeEmployeeID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_TypeEmployeeID] ON [dbo].[Employee]
(
	[TypeEmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_EmployeeID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_EmployeeID] ON [dbo].[Salary]
(
	[EmployeeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ClientID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ClientID] ON [dbo].[SeasonTicket]
(
	[ClientID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CoachID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_CoachID] ON [dbo].[SeasonTicket]
(
	[CoachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ServiceID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ServiceID] ON [dbo].[SeasonTicket]
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_CoachID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_CoachID] ON [dbo].[TrainingAppointments]
(
	[CoachID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_LoungeID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_LoungeID] ON [dbo].[TrainingAppointments]
(
	[LoungeID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ServiceID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ServiceID] ON [dbo].[TrainingAppointments]
(
	[ServiceID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_ID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_ID] ON [dbo].[UserSystem]
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_SeasonTicketID]    Script Date: 25.12.2020 17:26:08 ******/
CREATE NONCLUSTERED INDEX [IX_SeasonTicketID] ON [dbo].[Visitings]
(
	[SeasonTicketID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Client] ADD  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[Services] ADD  DEFAULT ((0)) FOR [isDeleted]
GO
ALTER TABLE [dbo].[TrainingAppointments] ADD  DEFAULT ((0)) FOR [IsCanceled]
GO
ALTER TABLE [dbo].[Activity]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Activity_dbo.Coaches_CoachID] FOREIGN KEY([CoachID])
REFERENCES [dbo].[Coaches] ([ID])
GO
ALTER TABLE [dbo].[Activity] CHECK CONSTRAINT [FK_dbo.Activity_dbo.Coaches_CoachID]
GO
ALTER TABLE [dbo].[Activity]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Activity_dbo.Services_ServiceID] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[Services] ([ID])
GO
ALTER TABLE [dbo].[Activity] CHECK CONSTRAINT [FK_dbo.Activity_dbo.Services_ServiceID]
GO
ALTER TABLE [dbo].[AppointmentClientTrainings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AppointmentClientTrainings_dbo.SeasonTicket_SeasonTicketID] FOREIGN KEY([SeasonTicketID])
REFERENCES [dbo].[SeasonTicket] ([ID])
GO
ALTER TABLE [dbo].[AppointmentClientTrainings] CHECK CONSTRAINT [FK_dbo.AppointmentClientTrainings_dbo.SeasonTicket_SeasonTicketID]
GO
ALTER TABLE [dbo].[AppointmentClientTrainings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AppointmentClientTrainings_dbo.TrainingAppointments_TrainingAppointmentID] FOREIGN KEY([TrainingAppointmentID])
REFERENCES [dbo].[TrainingAppointments] ([ID])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AppointmentClientTrainings] CHECK CONSTRAINT [FK_dbo.AppointmentClientTrainings_dbo.TrainingAppointments_TrainingAppointmentID]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Client_dbo.Human_ID] FOREIGN KEY([ID])
REFERENCES [dbo].[Human] ([ID])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_dbo.Client_dbo.Human_ID]
GO
ALTER TABLE [dbo].[Coaches]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Coaches_dbo.Employee_ID] FOREIGN KEY([ID])
REFERENCES [dbo].[Employee] ([ID])
GO
ALTER TABLE [dbo].[Coaches] CHECK CONSTRAINT [FK_dbo.Coaches_dbo.Employee_ID]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Employee_dbo.Human_ID] FOREIGN KEY([ID])
REFERENCES [dbo].[Human] ([ID])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_dbo.Employee_dbo.Human_ID]
GO
ALTER TABLE [dbo].[Employee]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Employee_dbo.TypeEmployees_TypeEmployeeID] FOREIGN KEY([TypeEmployeeID])
REFERENCES [dbo].[TypeEmployees] ([ID])
GO
ALTER TABLE [dbo].[Employee] CHECK CONSTRAINT [FK_dbo.Employee_dbo.TypeEmployees_TypeEmployeeID]
GO
ALTER TABLE [dbo].[Salary]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Salary_dbo.Employee_EmployeeID] FOREIGN KEY([EmployeeID])
REFERENCES [dbo].[Employee] ([ID])
GO
ALTER TABLE [dbo].[Salary] CHECK CONSTRAINT [FK_dbo.Salary_dbo.Employee_EmployeeID]
GO
ALTER TABLE [dbo].[SeasonTicket]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SeasonTicket_dbo.Client_ClientID] FOREIGN KEY([ClientID])
REFERENCES [dbo].[Client] ([ID])
GO
ALTER TABLE [dbo].[SeasonTicket] CHECK CONSTRAINT [FK_dbo.SeasonTicket_dbo.Client_ClientID]
GO
ALTER TABLE [dbo].[SeasonTicket]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SeasonTicket_dbo.Coaches_CoachID] FOREIGN KEY([CoachID])
REFERENCES [dbo].[Coaches] ([ID])
GO
ALTER TABLE [dbo].[SeasonTicket] CHECK CONSTRAINT [FK_dbo.SeasonTicket_dbo.Coaches_CoachID]
GO
ALTER TABLE [dbo].[SeasonTicket]  WITH CHECK ADD  CONSTRAINT [FK_dbo.SeasonTicket_dbo.Services_ServiceID] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[Services] ([ID])
GO
ALTER TABLE [dbo].[SeasonTicket] CHECK CONSTRAINT [FK_dbo.SeasonTicket_dbo.Services_ServiceID]
GO
ALTER TABLE [dbo].[TrainingAppointments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Coaches_CoachID] FOREIGN KEY([CoachID])
REFERENCES [dbo].[Coaches] ([ID])
GO
ALTER TABLE [dbo].[TrainingAppointments] CHECK CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Coaches_CoachID]
GO
ALTER TABLE [dbo].[TrainingAppointments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Lounge_LoungeID] FOREIGN KEY([LoungeID])
REFERENCES [dbo].[Lounge] ([ID])
GO
ALTER TABLE [dbo].[TrainingAppointments] CHECK CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Lounge_LoungeID]
GO
ALTER TABLE [dbo].[TrainingAppointments]  WITH CHECK ADD  CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Services_ServiceID] FOREIGN KEY([ServiceID])
REFERENCES [dbo].[Services] ([ID])
GO
ALTER TABLE [dbo].[TrainingAppointments] CHECK CONSTRAINT [FK_dbo.TrainingAppointments_dbo.Services_ServiceID]
GO
ALTER TABLE [dbo].[UserSystem]  WITH CHECK ADD  CONSTRAINT [FK_dbo.UserSystem_dbo.Employee_ID] FOREIGN KEY([ID])
REFERENCES [dbo].[Employee] ([ID])
GO
ALTER TABLE [dbo].[UserSystem] CHECK CONSTRAINT [FK_dbo.UserSystem_dbo.Employee_ID]
GO
ALTER TABLE [dbo].[Visitings]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Visitings_dbo.SeasonTicket_SeasonTicketID] FOREIGN KEY([SeasonTicketID])
REFERENCES [dbo].[SeasonTicket] ([ID])
GO
ALTER TABLE [dbo].[Visitings] CHECK CONSTRAINT [FK_dbo.Visitings_dbo.SeasonTicket_SeasonTicketID]
GO
USE [master]
GO
ALTER DATABASE [FitnessСlub] SET  READ_WRITE 
GO
