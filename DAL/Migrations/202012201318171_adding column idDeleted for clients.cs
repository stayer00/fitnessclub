﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnidDeletedforclients : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Client", "isDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Client", "isDeleted");
        }
    }
}
