﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changingnullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SeasonTicket", "classesLeft", c => c.Int(nullable: false));
            AlterColumn("dbo.SeasonTicket", "saleClasses", c => c.Int(nullable: false));
            AlterColumn("dbo.SeasonTicket", "saleConstantClient", c => c.Int(nullable: false));
            AlterColumn("dbo.SeasonTicket", "saleAge", c => c.Int(nullable: false));
            AlterColumn("dbo.SeasonTicket", "totalCost", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SeasonTicket", "totalCost", c => c.Single());
            AlterColumn("dbo.SeasonTicket", "saleAge", c => c.Int());
            AlterColumn("dbo.SeasonTicket", "saleConstantClient", c => c.Int());
            AlterColumn("dbo.SeasonTicket", "saleClasses", c => c.Int());
            AlterColumn("dbo.SeasonTicket", "classesLeft", c => c.Int());
        }
    }
}
