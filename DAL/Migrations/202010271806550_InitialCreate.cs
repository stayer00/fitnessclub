﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Activity",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        CoachID = c.Int(nullable: false),
                        ServiceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Coaches", t => t.CoachID)
                .ForeignKey("dbo.Services", t => t.ServiceID)
                .Index(t => t.CoachID)
                .Index(t => t.ServiceID);
            
            CreateTable(
                "dbo.Coaches",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        dateStartWorkExpirience = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employee", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.Employee",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        address = c.String(nullable: false),
                        getStarted = c.DateTime(nullable: false, storeType: "date"),
                        baseSalary = c.Single(nullable: false),
                        employmentRecordNumber = c.String(nullable: false),
                        ITN = c.String(nullable: false),
                        passport = c.String(nullable: false),
                        snils = c.String(nullable: false),
                        TypeEmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Human", t => t.ID)
                .ForeignKey("dbo.TypeEmployees", t => t.TypeEmployeeID)
                .Index(t => t.ID)
                .Index(t => t.TypeEmployeeID);
            
            CreateTable(
                "dbo.Human",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        surname = c.String(nullable: false),
                        birthday = c.DateTime(nullable: false, storeType: "date"),
                        gender = c.String(nullable: false, maxLength: 10, fixedLength: true),
                        numberPhone = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Client",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        dateRegistry = c.DateTime(nullable: false, storeType: "date"),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Human", t => t.ID)
                .Index(t => t.ID);
            
            CreateTable(
                "dbo.SeasonTicket",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ServiceID = c.Int(nullable: false),
                        ClientID = c.Int(nullable: false),
                        CoachID = c.Int(),
                        classesTotal = c.Int(nullable: false),
                        classesLeft = c.Int(),
                        dateStart = c.DateTime(nullable: false, storeType: "date"),
                        dateEnd = c.DateTime(nullable: false, storeType: "date"),
                        saleClasses = c.Int(),
                        saleConstantClient = c.Int(),
                        saleAge = c.Int(),
                        totalCost = c.Single(),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Services", t => t.ServiceID)
                .ForeignKey("dbo.Coaches", t => t.CoachID)
                .ForeignKey("dbo.Client", t => t.ClientID)
                .Index(t => t.ServiceID)
                .Index(t => t.ClientID)
                .Index(t => t.CoachID);
            
            CreateTable(
                "dbo.AppointmentClientTrainings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        TrainingAppointmentID = c.Int(nullable: false),
                        SeasonTicketID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.TrainingAppointments", t => t.TrainingAppointmentID)
                .ForeignKey("dbo.SeasonTicket", t => t.SeasonTicketID)
                .Index(t => t.TrainingAppointmentID)
                .Index(t => t.SeasonTicketID);
            
            CreateTable(
                "dbo.TrainingAppointments",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        maxCount = c.Int(nullable: false),
                        ServiceID = c.Int(nullable: false),
                        CoachID = c.Int(nullable: false),
                        LoungeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Lounge", t => t.LoungeID)
                .ForeignKey("dbo.Services", t => t.ServiceID)
                .ForeignKey("dbo.Coaches", t => t.CoachID)
                .Index(t => t.ServiceID)
                .Index(t => t.CoachID)
                .Index(t => t.LoungeID);
            
            CreateTable(
                "dbo.Lounge",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false),
                        maxCount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        name = c.String(),
                        cost = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Visitings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        date = c.DateTime(nullable: false),
                        SeasonTicketID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.SeasonTicket", t => t.SeasonTicketID)
                .Index(t => t.SeasonTicketID);
            
            CreateTable(
                "dbo.Salary",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        year = c.Int(nullable: false),
                        month = c.Int(nullable: false),
                        salary = c.Single(nullable: false),
                        EmployeeID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employee", t => t.EmployeeID)
                .Index(t => t.EmployeeID);
            
            CreateTable(
                "dbo.TypeEmployees",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.UserSystem",
                c => new
                    {
                        ID = c.Int(nullable: false),
                        email = c.String(nullable: false),
                        password = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.Employee", t => t.ID)
                .Index(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TrainingAppointments", "CoachID", "dbo.Coaches");
            DropForeignKey("dbo.UserSystem", "ID", "dbo.Employee");
            DropForeignKey("dbo.Employee", "TypeEmployeeID", "dbo.TypeEmployees");
            DropForeignKey("dbo.Salary", "EmployeeID", "dbo.Employee");
            DropForeignKey("dbo.Employee", "ID", "dbo.Human");
            DropForeignKey("dbo.Client", "ID", "dbo.Human");
            DropForeignKey("dbo.SeasonTicket", "ClientID", "dbo.Client");
            DropForeignKey("dbo.Visitings", "SeasonTicketID", "dbo.SeasonTicket");
            DropForeignKey("dbo.SeasonTicket", "CoachID", "dbo.Coaches");
            DropForeignKey("dbo.AppointmentClientTrainings", "SeasonTicketID", "dbo.SeasonTicket");
            DropForeignKey("dbo.TrainingAppointments", "ServiceID", "dbo.Services");
            DropForeignKey("dbo.SeasonTicket", "ServiceID", "dbo.Services");
            DropForeignKey("dbo.Activity", "ServiceID", "dbo.Services");
            DropForeignKey("dbo.TrainingAppointments", "LoungeID", "dbo.Lounge");
            DropForeignKey("dbo.AppointmentClientTrainings", "TrainingAppointmentID", "dbo.TrainingAppointments");
            DropForeignKey("dbo.Coaches", "ID", "dbo.Employee");
            DropForeignKey("dbo.Activity", "CoachID", "dbo.Coaches");
            DropIndex("dbo.UserSystem", new[] { "ID" });
            DropIndex("dbo.Salary", new[] { "EmployeeID" });
            DropIndex("dbo.Visitings", new[] { "SeasonTicketID" });
            DropIndex("dbo.TrainingAppointments", new[] { "LoungeID" });
            DropIndex("dbo.TrainingAppointments", new[] { "CoachID" });
            DropIndex("dbo.TrainingAppointments", new[] { "ServiceID" });
            DropIndex("dbo.AppointmentClientTrainings", new[] { "SeasonTicketID" });
            DropIndex("dbo.AppointmentClientTrainings", new[] { "TrainingAppointmentID" });
            DropIndex("dbo.SeasonTicket", new[] { "CoachID" });
            DropIndex("dbo.SeasonTicket", new[] { "ClientID" });
            DropIndex("dbo.SeasonTicket", new[] { "ServiceID" });
            DropIndex("dbo.Client", new[] { "ID" });
            DropIndex("dbo.Employee", new[] { "TypeEmployeeID" });
            DropIndex("dbo.Employee", new[] { "ID" });
            DropIndex("dbo.Coaches", new[] { "ID" });
            DropIndex("dbo.Activity", new[] { "ServiceID" });
            DropIndex("dbo.Activity", new[] { "CoachID" });
            DropTable("dbo.UserSystem");
            DropTable("dbo.TypeEmployees");
            DropTable("dbo.Salary");
            DropTable("dbo.Visitings");
            DropTable("dbo.Services");
            DropTable("dbo.Lounge");
            DropTable("dbo.TrainingAppointments");
            DropTable("dbo.AppointmentClientTrainings");
            DropTable("dbo.SeasonTicket");
            DropTable("dbo.Client");
            DropTable("dbo.Human");
            DropTable("dbo.Employee");
            DropTable("dbo.Coaches");
            DropTable("dbo.Activity");
        }
    }
}
