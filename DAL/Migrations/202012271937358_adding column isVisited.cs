﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnisVisited : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AppointmentClientTrainings", "isVisited", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.AppointmentClientTrainings", "isVisited");
        }
    }
}
