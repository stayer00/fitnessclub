﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnisDeletedforemployee : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Employee", "isDeleted", c => c.Boolean(nullable: true));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Employee", "isDeleted");
        }
    }
}
