﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class check : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Employee", "isDeleted", c => c.Boolean());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Employee", "isDeleted", c => c.Boolean(nullable: false));
        }
    }
}
