﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addingcolumnidDeletedforservice : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Services", "isDeleted", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Services", "isDeleted");
        }
    }
}
