﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class delete_cascade_training_appointment : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.AppointmentClientTrainings", "TrainingAppointmentID", "dbo.TrainingAppointments");
            AddForeignKey("dbo.AppointmentClientTrainings", "TrainingAppointmentID", "dbo.TrainingAppointments", "ID", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AppointmentClientTrainings", "TrainingAppointmentID", "dbo.TrainingAppointments");
            AddForeignKey("dbo.AppointmentClientTrainings", "TrainingAppointmentID", "dbo.TrainingAppointments", "ID");
        }
    }
}
