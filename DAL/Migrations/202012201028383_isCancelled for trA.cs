﻿namespace DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class isCancelledfortrA : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TrainingAppointments", "IsCanceled", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TrainingAppointments", "IsCanceled");
        }
    }
}
