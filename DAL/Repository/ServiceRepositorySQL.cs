﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class ServiceRepositorySQL : IRepositoryService, IGetLast<Service>
    {
        private FitnessClubModel db;

        public ServiceRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void Create(Service service)
        {
            db.Services.Add(service);
        }

        public void Delete(int id)
        {
            Service service = db.Services.Find(id);
            if (service != null)
            {
                service.isDeleted = true;
                db.Entry(service).State = EntityState.Modified;
                //db.Services.Remove(service);
            }
        }

        public Service GetItem(int id)
        {
            return db.Services.Find(id);
        }

        public Service GetLast()
        {
            return db.Services.OrderByDescending(u => u.ID).FirstOrDefault();
        }

        public List<Service> GetList()
        {
            return db.Services.Where(i => i.isDeleted == false).ToList();
        }

        public void Update(Service service)
        {
            db.Entry(service).State = EntityState.Modified;
        }
    }
}
