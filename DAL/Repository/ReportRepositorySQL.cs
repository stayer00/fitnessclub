﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class ReportRepositorySQL : IReportRepository
    {
        private FitnessClubModel db;

        public ReportRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }
        public List<SeasonTicket> GetSTwithServices(int year)
        {
            return db.SeasonTickets
                .Join(db.Services, st => st.ServiceID, s => s.ID, (st, s) => st)
                .Where(st => st.dateStart.Year == year)
                .ToList();
        }
    }
}
