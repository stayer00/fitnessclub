﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class SeasonTicketRepositorySQL : IRepositorySeasonTicket
    {
        private FitnessClubModel db;

        public SeasonTicketRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void AddVisiting(int SeasonTicketID)
        {
            db.Visitings.Add(new Visiting() { SeasonTicketID = SeasonTicketID, date = DateTime.Now.Date });
            db.SeasonTickets.Find(SeasonTicketID).classesLeft--;
        }

        public void CreateSeasonTicket(SeasonTicket st)
        {
            db.SeasonTickets.Add(st);
        }

        public List<SeasonTicket> GetActiveSeasonTicketByService(int ServiceID)
        {
            return db.SeasonTickets
               .Where(i =>
                   i.classesLeft != 0 &&
                   i.dateEnd >= DateTime.Today &&
                   i.ServiceID == ServiceID)
               .ToList();
        }

        public IQueryable<IGrouping<int, SeasonTicket>> GetClientSeasonTicket(int clientId)
        {
            var req = from st in db.SeasonTickets
                   join v in db.Visitings on st.ID equals v.SeasonTicketID into g
                   join c in db.Coaches on st.CoachID equals c.ID into g1
                   from res in g.DefaultIfEmpty()
                   where st.ClientID == clientId
                   group st by st.ID;
            return req;
        }
    }
}
