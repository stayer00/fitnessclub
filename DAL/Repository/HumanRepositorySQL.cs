﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class HumanRepositorySQL : IRepositoryHuman
    {
        private FitnessClubModel db;

        public HumanRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void Create(Human human)
        {
            db.Humen.Add(human);
        }

        public void Delete(int id)
        {
            Human human = db.Humen.Find(id);
            if (human != null)
                db.Humen.Remove(human);
        }

        public Human GetItem(int id)
        {
            return db.Humen.Find(id);
        }

        public Human GetLast()
        {
            return db.Humen.OrderByDescending(u => u.ID).FirstOrDefault();
        }

        public List<Human> GetList()
        {
            throw new NotImplementedException();
        }

        public void Update(Human item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
