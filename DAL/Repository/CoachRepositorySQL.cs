﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class CoachRepositorySQL: IRepositoryCoach
    {
        private FitnessClubModel db;

        public CoachRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void Delete(int id)
        {
            Coach coach = db.Coaches.Find(id);
            if (coach != null)
                db.Coaches.Remove(coach);
        }

        public void AddCoachActivity(int idService, int idCoach)
        {
            db.Activities.Add(new Activity()
            {
                CoachID = idCoach,
                ServiceID = idService
            });
        }

        public List<Activity> GetCoachActivities(int idCoach)
        {
            return db.Activities.Where(i => i.CoachID == idCoach).ToList();
        }
        public void DeleteCoachActivities(int idCoach)
        {
            db.Activities.RemoveRange(GetCoachActivities(idCoach));
        }

        public void DeleteCoachActivity(int idService, int idCoach)
        {
            var coachActivity = db.Activities.Where(i => i.CoachID == idCoach && i.ServiceID == idService).FirstOrDefault();
            db.Activities.Remove(coachActivity);
        }

        public List<Coach> GetList()
        {
            return db.Coaches.ToList().Where(i => i.Employee.isDeleted != true).ToList();
        }

        public void Create(Coach item)
        {
            db.Coaches.Add(item);
        }

        public void Update(Coach item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public bool isCoachFree(DateTime dateTime, int idCoach)
        {
            return db.TrainingAppointments.Where(i => i.date >= dateTime && i.date <= dateTime.AddHours(2) && i.CoachID == idCoach).FirstOrDefault() == null;
        }

        public Coach GetItem(int? coachID)
        {
            if (coachID != null)
            {
                return db.Coaches.Find(coachID);
            }
            return null;
        }

        public Coach GetItem(int id)
        {
            return db.Coaches.Find(id);
        }
    }
}
