﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class DbReposSQL: IDbRepos
    {
        private FitnessClubModel db;
        private ClientRepositorySQL clientRepository;
        private HumanRepositorySQL humanRepository;
        private EmployeeRepositorySQL employeeRepository;
        private SeasonTicketRepositorySQL seasonTicketRepository;
        private TrainingAppointmentRepositorySQL traininingAppointmentRepository;
        private ServiceRepositorySQL serviceRepository;
        private CoachRepositorySQL coachRepository;
        private ReportRepositorySQL reportRepository;
        private LoungeRepositorySQL loungeRepository;

        public DbReposSQL()
        {
            db = new FitnessClubModel();
        }

        public IRepositoryClient Clients
        {
            get
            {
                if (clientRepository == null)
                    clientRepository = new ClientRepositorySQL(db);
                return clientRepository;
            }
        }

        public IRepositoryHuman Human
        {
            get
            {
                if (humanRepository == null)
                    humanRepository = new HumanRepositorySQL(db);
                return humanRepository;
            }
        }

        public IRepositoryEmployee Employees
        {
            get
            {
                if (employeeRepository == null)
                    employeeRepository = new EmployeeRepositorySQL(db);
                return employeeRepository;
            }
        }

        public IRepositorySeasonTicket SeasonTickets
        {
            get
            {
                if (seasonTicketRepository == null)
                    seasonTicketRepository = new SeasonTicketRepositorySQL(db);
                return seasonTicketRepository;
            }
        }

        public IRepositoryTrainingAppointment TrainingAppointments
        {
            get
            {
                if (traininingAppointmentRepository == null)
                    traininingAppointmentRepository = new TrainingAppointmentRepositorySQL(db);
                return traininingAppointmentRepository;
            }
        }

        public IReportRepository Report
        {
            get
            {
                if (reportRepository == null)
                    reportRepository = new ReportRepositorySQL(db);
                return reportRepository;
            }
        }

        public IRepositoryCoach Coaches
        {
            get
            {
                if (coachRepository == null)
                    coachRepository = new CoachRepositorySQL(db);
                return coachRepository;
            }
        }
        public IRepositoryLounge Lounges
        {
            get
            {
                if (loungeRepository == null)
                    loungeRepository = new LoungeRepositorySQL(db);
                return loungeRepository;
            }
        }

        public IRepositoryService Services
        {
            get
            {
                if (serviceRepository == null)
                    serviceRepository = new ServiceRepositorySQL(db);
                return serviceRepository;
            }
        }

        public int Save()
        {
            try
            {
                return db.SaveChanges();
            }
            catch (Exception)
            {
            }
            return 0;
        }
    }
}