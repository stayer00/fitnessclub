﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class LoungeRepositorySQL : IRepositoryLounge
    {
        private FitnessClubModel db;

        public LoungeRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void Create(Lounge item)
        {
            db.Lounges.Add(item);
        }

        public void Delete(int id)
        {
            Lounge lounge = db.Lounges.Find(id);
            if (lounge != null)
                db.Lounges.Remove(lounge);
        }

        public Lounge GetItem(int id)
        {
            return db.Lounges.Find(id);
        }

        public List<Lounge> GetList()
        {
            return db.Lounges.ToList();
        }

        public bool isLoungeFree(DateTime dateTime, int idLounge)
        {
            return db.TrainingAppointments.Where(i => i.date >= dateTime && i.date <= dateTime.AddHours(2) && i.LoungeID == idLounge).FirstOrDefault() == null;
        }

        public void Update(Lounge item)
        {
            db.Entry(item).State = EntityState.Modified;
        }
    }
}
