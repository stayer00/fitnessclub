﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class TrainingAppointmentRepositorySQL : IRepositoryTrainingAppointment
    {
        private FitnessClubModel db;

        public TrainingAppointmentRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void AddClientToTraining(int SeasonTicketID, int TrainingAppointmentID)
        {
            var lastAppointment = db.AppointmentClientTrainings.OrderByDescending(u => u.ID).FirstOrDefault();
            int maxId = lastAppointment == null ? 1 : lastAppointment.ID + 1;
            db.AppointmentClientTrainings.Add(new AppointmentClientTraining() { ID = maxId, SeasonTicketID = SeasonTicketID, TrainingAppointmentID = TrainingAppointmentID });
        }

        public void Create(TrainingAppointment tr)
        {
            db.TrainingAppointments.Add(new TrainingAppointment() { CoachID = tr.CoachID, LoungeID = tr.LoungeID, ServiceID = tr.ServiceID, maxCount = tr.maxCount, date = tr.date });
        }

        public void Delete(int id)
        {
            var trA = db.TrainingAppointments.Where(i => i.ID == id).FirstOrDefault();
            db.TrainingAppointments.Remove(trA);
        }

        public void DeleteClientTrainingAppointment(int TrainingAppointmentID, int clientId)
        {
            var ap = db.AppointmentClientTrainings.Where(i => i.TrainingAppointmentID == TrainingAppointmentID && i.SeasonTicketID == clientId).FirstOrDefault();
            db.AppointmentClientTrainings.Remove(ap);
        }

        public TrainingAppointment GetItem(int id)
        {
            return db.TrainingAppointments.Find(id);
        }

        public TrainingAppointment GetLast()
        {
            return db.TrainingAppointments.OrderByDescending(i => i.ID).FirstOrDefault();
        }

        public List<TrainingAppointment> GetList()
        {
            throw new NotImplementedException();
        }

        public List<AppointmentClientTraining> GetTrainingAppointmentClients(int TrainingAppointmentID)
        {
            throw new NotImplementedException();
        }

        public IQueryable<IGrouping<int, TrainingAppointment>> GetTrainingAppointments(DateTime date)
        {
            return from ta in db.TrainingAppointments
                   join tac in db.AppointmentClientTrainings on ta.ID equals tac.TrainingAppointmentID into g
                   from res in g.DefaultIfEmpty()
                   where EntityFunctions.TruncateTime(ta.date) == date.Date
                   group ta by ta.ID;
        }

        public List<TrainingAppointment> GetTrainingsDay(DateTime date)
        {
            return db.TrainingAppointments.Where(i => EntityFunctions.TruncateTime(i.date) == date).ToList();
        }

        public void ToggleCancelTrA(int id)
        {
            var trA = this.GetItem(id);
            if (trA != null)
            {
                trA.IsCanceled = true;
                db.Entry(trA).State = EntityState.Modified;
            }
        }

        public void Update(TrainingAppointment item)
        {
            throw new NotImplementedException();
        }
    }
}
