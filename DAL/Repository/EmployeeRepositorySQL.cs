﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class EmployeeRepositorySQL : IRepositoryEmployee
    {
        private FitnessClubModel db;

        public EmployeeRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public void Create(Employee item)
        {
            db.Employees.Add(item);
        }

        public void Delete(int id)
        {
            Employee employee = db.Employees.Find(id);
            if (employee != null)
            {
                employee.isDeleted = true;
                db.Entry(employee).State = EntityState.Modified;
            }
        }

        public Employee GetItem(int id)
        {
            return db.Employees.Find(id);
        }

        public List<Employee> GetList()
        {
            return db.Employees.ToList().Where(i => i.isDeleted != true).ToList();
        }

        public List<TypeEmployee> GetEmployeeTypes()
        {
            return db.TypeEmployees.ToList();
        }

        public void Update(Employee item)
        {
            db.Entry(item).State = EntityState.Modified;
        }

        public List<Salary> DeliverSalary()
        {
            var Salaries = new List<Salary>();
            foreach (var employee in this.GetList())
            {
                double salary = employee.baseSalary;
                if (employee.TypeEmployeeID == 1)
                {
                    var services =
                        db.Visitings.Join(db.SeasonTickets, v => v.SeasonTicketID, st => st.ID, (v, st) => new { v = v, st = st })
                        .Where(i => i.st.CoachID == employee.ID
                            && i.v.date.Year == DateTime.Today.Year
                            && i.v.date.Month == DateTime.Today.Month)
                        .Join(db.Services, st => st.st.ServiceID, s => s.ID, (st, s) => new { st = st, s = s })
                        .ToList();

                    if (services.Count != 0)
                    {
                        var sal = services
                        .Sum(i => i.s.cost * 0.3);
                        salary += sal;
                    }
                }

                var newSalary = new Salary()
                {
                    EmployeeID = employee.ID,
                    month = DateTime.Today.Month,
                    year = DateTime.Today.Year,
                    salary1 = (float)salary
                };
                if (employee.Human.surname != "-")
                {
                    Salaries.Add(newSalary);
                }
                db.Salaries.Add(newSalary);
            }

            return Salaries;
        }

        public void CreateSystemUser(UserSystem user)
        {
            db.UserSystems.Add(user);
        }

        public UserSystem GetSystemUser(string login, string password)
        {
            return db.UserSystems.Where(i => i.password == password && i.email == login).FirstOrDefault();
        }
    }
}
