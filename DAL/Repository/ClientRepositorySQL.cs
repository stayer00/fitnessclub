﻿using DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository
{
    public class ClientRepositorySQL : IRepositoryClient
    {
        private FitnessClubModel db;

        public ClientRepositorySQL(FitnessClubModel dbcontext)
        {
            this.db = dbcontext;
        }

        public List<Client> GetList()
        {
            return db.Clients.Where(i => i.isDeleted == false).ToList();
        }

        public Client GetItem(int id)
        {
            return db.Clients.Find(id);
        }

        public void Create(Client client)
        {
            db.Clients.Add(client);
        }

        public void Update(Client client)
        {
            db.Entry(client).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Client client = db.Clients.Find(id);
            if (client != null)
            {
                //db.Clients.Remove(client);
                client.isDeleted = true;
                db.Entry(client).State = EntityState.Modified;
            }
        }

        public int GetClientVisiting(int ClientID)
        {
            return db.Clients
                .Where(i => i.ID == ClientID)
                .Join(db.SeasonTickets, c => c.ID, s => s.ClientID, (cl, s) => s)
                .Join(db.Visitings, s => s.ID, v => v.SeasonTicketID, (s, v) => v)
                .Count();
        }
    }
}
