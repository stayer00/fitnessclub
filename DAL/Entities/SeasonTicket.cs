﻿namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SeasonTicket")]
    public partial class SeasonTicket
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SeasonTicket()
        {
            Visitings = new HashSet<Visiting>();
            AppointmentClientTrainings = new HashSet<AppointmentClientTraining>();
        }

        public int ID { get; set; }

        public int ServiceID { get; set; }

        public int ClientID { get; set; }

        public int? CoachID { get; set; }

        public int classesTotal { get; set; }

        public int classesLeft { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateStart { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateEnd { get; set; }

        public int saleClasses { get; set; }

        public int saleConstantClient { get; set; }

        public int saleAge { get; set; }

        public float totalCost { get; set; }

        public virtual Client Client { get; set; }

        public virtual Coach Coach { get; set; }

        public virtual Service Service { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Visiting> Visitings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppointmentClientTraining> AppointmentClientTrainings { get; set; }
    }
}

