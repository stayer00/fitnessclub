namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserSystem")]
    public partial class UserSystem
    {
        [ForeignKey("Employee")]
        public int ID { get; set; }

        [Required]
        public string email { get; set; }

        [Required]
        public string password { get; set; }

        public virtual Employee Employee { get; set; }
    }
}
