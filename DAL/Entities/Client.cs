namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Client")]
    public partial class Client
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Client()
        {
            SeasonTickets = new HashSet<SeasonTicket>();
        }

        [ForeignKey("Human")]
        public int ID { get; set; }

        [Column(TypeName = "date")]
        public DateTime dateRegistry { get; set; }
        public bool isDeleted { get; set; }

        public virtual Human Human { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SeasonTicket> SeasonTickets { get; set; }
    }
}
