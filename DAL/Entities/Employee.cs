namespace DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Employee")]
    public partial class Employee
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Employee()
        {
            Salaries = new HashSet<Salary>();
        }

        [ForeignKey("Human")]
        public int ID { get; set; }

        [Required]
        public string address { get; set; }

        [Column(TypeName = "date")]
        public DateTime getStarted { get; set; }

        public float baseSalary { get; set; }

        public bool? isDeleted { get; set; }

        [Required]
        public string employmentRecordNumber { get; set; }

        [Required]
        public string ITN { get; set; }

        [Required]
        public string passport { get; set; }

        [Required]
        public string snils { get; set; }

        public int TypeEmployeeID { get; set; }

        public virtual Coach Coach { get; set; }

        public virtual Human Human { get; set; }

        public virtual TypeEmployee TypeEmployee { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Salary> Salaries { get; set; }
        public virtual UserSystem UserSystem { get; set; }
    }
}
