namespace DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class FitnessClubModel : DbContext
    {
        public FitnessClubModel()
            : base("name=FitnessClubEntity")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public virtual DbSet<Activity> Activities { get; set; }
        public virtual DbSet<AppointmentClientTraining> AppointmentClientTrainings { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Coach> Coaches { get; set; }
        public virtual DbSet<Employee> Employees { get; set; }
        public virtual DbSet<Human> Humen { get; set; }
        public virtual DbSet<Lounge> Lounges { get; set; }
        public virtual DbSet<Salary> Salaries { get; set; }
        public virtual DbSet<SeasonTicket> SeasonTickets { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<TrainingAppointment> TrainingAppointments { get; set; }
        public virtual DbSet<TypeEmployee> TypeEmployees { get; set; }
        public virtual DbSet<UserSystem> UserSystems { get; set; }
        public virtual DbSet<Visiting> Visitings { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>()
                .HasMany(e => e.SeasonTickets)
                .WithRequired(e => e.Client)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Coach>()
                .HasMany(e => e.Activities)
                .WithRequired(e => e.Coach)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Coach>()
                .HasMany(e => e.TrainingAppointments)
                .WithRequired(e => e.Coach)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Employee>()
                .HasOptional(e => e.Coach)
                .WithRequired(e => e.Employee);

            modelBuilder.Entity<Employee>()
               .HasOptional(e => e.UserSystem)
               .WithRequired(e => e.Employee);

            modelBuilder.Entity<Employee>()
                .HasMany(e => e.Salaries)
                .WithRequired(e => e.Employee)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Human>()
                .Property(e => e.gender)
                .IsFixedLength();

            modelBuilder.Entity<Human>()
                .HasOptional(e => e.Client)
                .WithRequired(e => e.Human);

            modelBuilder.Entity<Human>()
                .HasOptional(e => e.Employee)
                .WithRequired(e => e.Human);

            modelBuilder.Entity<Lounge>()
                .HasMany(e => e.TrainingAppointments)
                .WithRequired(e => e.Lounge)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SeasonTicket>()
                .HasMany(e => e.AppointmentClientTrainings)
                .WithRequired(e => e.SeasonTicket)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SeasonTicket>()
                .HasMany(e => e.Visitings)
                .WithRequired(e => e.SeasonTicket)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.Activities)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.SeasonTickets)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Service>()
                .HasMany(e => e.TrainingAppointments)
                .WithRequired(e => e.Service)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TrainingAppointment>()
                .HasMany(e => e.AppointmentClientTrainings)
                .WithRequired(e => e.TrainingAppointment)
                .WillCascadeOnDelete(true);

            modelBuilder.Entity<TypeEmployee>()
                .HasMany(e => e.Employees)
                .WithRequired(e => e.TypeEmployee)
                .WillCascadeOnDelete(false);
        }
    }
}
