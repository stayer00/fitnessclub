﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositoryCoach: IRepository<Coach>
    {
        void AddCoachActivity(int idService, int idCoach);
        void DeleteCoachActivity(int idService, int idCoach);
        bool isCoachFree(DateTime dateTime, int idCoach);
        Coach GetItem(int? coachID);
        List<Activity> GetCoachActivities(int idCoach);
        void DeleteCoachActivities(int idCoach);
    }
}
