﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositorySeasonTicket
    {
        IQueryable<IGrouping<int, SeasonTicket>> GetClientSeasonTicket(int clientId);
        void CreateSeasonTicket(SeasonTicket st);
        List<SeasonTicket> GetActiveSeasonTicketByService(int ServiceID);
        void AddVisiting(int SeasonTicketID);
    }
}
