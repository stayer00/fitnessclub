﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IDbRepos
    {
        IRepositoryClient Clients { get; }
        IRepositoryEmployee Employees { get; }
        IRepositorySeasonTicket SeasonTickets { get; }
        IRepositoryTrainingAppointment TrainingAppointments { get; }
        IRepositoryService Services { get; }
        IRepositoryCoach Coaches { get; }
        IRepositoryHuman Human { get; }
        IReportRepository Report { get; }
        IRepositoryLounge Lounges { get; }
        int Save();
    }
}
