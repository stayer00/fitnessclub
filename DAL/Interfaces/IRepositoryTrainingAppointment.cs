﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositoryTrainingAppointment: IGetLast<TrainingAppointment>, IRepository<TrainingAppointment>
    {
        IQueryable<IGrouping<int, TrainingAppointment>> GetTrainingAppointments(DateTime date);
        void AddClientToTraining(int SeasonTicketID, int TrainingAppointmentID);
        List<AppointmentClientTraining> GetTrainingAppointmentClients(int TrainingAppointmentID);
        void DeleteClientTrainingAppointment(int TrainingAppointmentID, int clientId);
        List<TrainingAppointment> GetTrainingsDay(DateTime date);
        void ToggleCancelTrA(int id);
    }
}
