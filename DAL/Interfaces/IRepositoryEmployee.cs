﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Interfaces
{
    public interface IRepositoryEmployee: IRepository<Employee>
    {
        List<TypeEmployee> GetEmployeeTypes();
        List<Salary> DeliverSalary();
        void CreateSystemUser(UserSystem user);
        UserSystem GetSystemUser(string login, string password);
    }
}
